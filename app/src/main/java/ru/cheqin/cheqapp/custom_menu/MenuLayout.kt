package ru.cheqin.cheqapp.custom_menu

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import ru.cheqin.cheqapp.main.MainActivity
import ru.cheqin.cheqapp.R

class MenuLayout(context: Context): LinearLayout(context) {

    init {
        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                56)
        layoutParams = params
        initView(context)

    }

    fun initView(context: Context) {
        val activity = context as MainActivity
        gravity = Gravity.CENTER_HORIZONTAL
        orientation = LinearLayout.HORIZONTAL
        LayoutInflater.from(context).inflate(R.layout.custom_menu_layout, this)
    }

}

