package ru.cheqin.cheqapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.empty_activity.*
import ru.cheqin.cheqapp.tools.buildQrBitmap
import ru.cheqin.cheqapp.scaner.ScannerActivity

class EmptyActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.empty_activity)

        val scan = intent.extras.getString(ScannerActivity.SCAN)

        imageView.setImageBitmap(buildQrBitmap(scan))
        textView.setText(scan)

    }


}