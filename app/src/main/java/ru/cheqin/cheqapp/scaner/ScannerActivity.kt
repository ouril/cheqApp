package ru.cheqin.cheqapp.scaner

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.github.kittinunf.fuel.httpPost
import com.github.salomonbrys.kotson.fromJson
import com.github.kittinunf.result.Result as Res
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.zxing.Result
import com.orhanobut.hawk.Hawk
import ru.cheqin.cheqapp.cheq.CheqActivity
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.jetbrains.anko.*
import ru.cheqin.cheqapp.EmptyActivity
import ru.cheqin.cheqapp.actions.OneActionActivity
import ru.cheqin.cheqapp.cheq.CheqView
import ru.cheqin.cheqapp.win.WinActivity
import ru.cheqin.cheqapp.tools.*


class ScannerActivity: AppCompatActivity(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)
        Hawk.init(this).build()
        // Set the scanner view as the content view
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()
    }

    companion object {
        val PRESENT_NAME = "PRESENT_NAME"
        val PRESENT_PIC ="PRESENT_PIC"
        val PROD_ID = "PROD_ID"
        val PRESENT_PROD = "PRESENT_PROD"
        val WIN = "WIN"
        val SCAN = "SCAN"
        val ISSPEC = "ISSPEC"
    }

    override fun handleResult(p0: Result?) {
        if (!p0!!.text.isNullOrBlank()) {

            if (linkVaslidator(p0.text)){
                if (isSpecialLinkValidator(p0.text)){
                    val specCode = p0.text.replace(SPECIAL_LINK, "")
                    checkGift(specCode, true)

                } else {
                    goToLink(p0)
                }
            } else if (qrDataValidator(p0.text)){

                startActivity<CheqView>(SCAN to p0.text)
                finish()
            } else {

                checkGift(p0.text)

            }
        }


    }

    fun goToLink(p0: Result) {
        alert("Перейти по ссылке?", p0.text) {
            yesButton {
                val address = Uri.parse(p0.text)
                val browserIntent = Intent(Intent.ACTION_VIEW, address)
                startActivity(browserIntent)
                finish()
            }
            noButton({
                finish()
            })
        }.show()
    }

    fun checkGift(scanRes:String?, isSpecialLink: Boolean = false) {
        Hawk.init(this).build()
        val userId = Hawk.get(PHONE, "None")

        /** there we get id of gift */

        val giftId = scanRes!!.replace(SPECIAL_LINK, "")
        val json = jsonObject(
                "method" to "bill_api.$CHECK_GIFT",
                "params" to jsonArray(
                        giftId,
                        userId
                ),
                "id" to 1
        )

        SERVER.httpPost().body(json.toString()).timeout(6000).responseString { request, response, result ->

            when (result) {
                is Res.Failure -> {
                    println("code ----> ${result.error}")
                    println("code ----> ${response.statusCode}")
                    println("code ----> ${response.responseMessage}")
                    toast("Что-то пошло не так, попробуйте еще раз.")
                    finish()
                }

                is Res.Success -> {

                        val gson = Gson()
                        val res = gson.fromJson<JsonObject>(result.get()).getAsJsonArray("result")

                        try {

                            val giftJson = res[0].asJsonObject
                            val type = giftJson.get("type").asString
                            if (type == "gift" || type == "promo" || type == "empty_gift") {
                                val winJson = giftJson.getAsJsonArray("data")

                                Log.d("RESULT_ARRAY", winJson.toString())

                                when (type) {
                                    "gift" -> {
                                        startActivity<WinActivity>(
                                                PRESENT_NAME to winJson[2].asString,
                                                PRESENT_PIC to winJson[3].asString,
                                                PRESENT_PROD to winJson[0].asString,
                                                PROD_ID to winJson[1].asString,
                                                ISSPEC to false,
                                                WIN to true
                                        )


                                    }
                                    "promo" -> {

                                        startActivity<OneActionActivity>(
                                                ID to winJson[0].asString
                                        )

                                    }
                                    "empty_gift" -> {
                                        startActivity<WinActivity>(
                                                PRESENT_NAME to "",
                                                PRESENT_PIC to "",
                                                PRESENT_PROD to "",
                                                ISSPEC to isSpecialLink,
                                                PROD_ID to winJson[0].asString,
                                                WIN to false
                                        )
                                    }

                                }
                                finish()
                            } else {

                               startActivity<EmptyActivity>(
                                       SCAN to scanRes
                               )


                            }


                        } catch (e: Exception) {

                            if (isSpecialLink){
                                startActivity<WinActivity>(
                                        PRESENT_NAME to "",
                                        PRESENT_PIC to "",
                                        PRESENT_PROD to "",
                                        PROD_ID to "",
                                        ISSPEC to true,
                                        WIN to false
                                )
                            } else {
                                startActivity<EmptyActivity>(
                                        SCAN to scanRes
                                )
                            }

                        }
                }
            }
        }
    }
}