package ru.cheqin.cheqapp.profile


import io.reactivex.Single
import ru.cheqin.cheqapp.AbsWebModel

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class ProfileModel: AbsWebModel(), ProfileContract.Model{




    override fun inputNumber(phoneNumber: String, uuid: String, url: String): Single<Answer> {
        return sendToServer(phoneNumber, uuid, "bill_api.send_verification_pin_sms", url)
    }

    override fun verefyCode(phoneNumber: String, verificationCode: String, url: String): Single<Answer> {
        return sendToServer(phoneNumber, verificationCode, "bill_api.signin_mobile_by_PIN", url)
    }

    fun sendToServer(phoneNumber: String, data: String, method: String, url: String): Single<Answer> {
        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val service = retrofit.create(RegisrationPost::class.java)
        val resp = Response(method, listOf(phoneNumber, data), 1)

        return service.callReg(resp)

    }







}


