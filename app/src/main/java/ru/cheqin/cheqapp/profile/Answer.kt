package ru.cheqin.cheqapp.profile

data class Error(val message: String, val code: Int)


data class Answer(val result: List<Boolean>?, val id: Int, val error: Error?)


data class Response(
        val method: String,
        val params: List<String>,
        val id: Int
)