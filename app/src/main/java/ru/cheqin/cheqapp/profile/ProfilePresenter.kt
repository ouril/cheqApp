package ru.cheqin.cheqapp.profile

import android.content.Context
import com.orhanobut.hawk.Hawk
import io.realm.Realm
import org.kodein.di.Kodein
import ru.cheqin.cheqapp.R
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import ru.cheqin.cheqapp.tools.ISREG
import ru.cheqin.cheqapp.tools.PHONE
import java.util.*

class ProfilePresenter(override val kodein: Kodein): KodeinAware, ProfileContract.Presenter{

    val realm: Realm by instance()
    val profileModel: ProfileModel by instance()
    lateinit var view: ProfileContract.View
    lateinit var model: ProfileContract.Model
    lateinit var context: Context

    override fun callSupport(phoneNumber: String, text: String) {
       //To change body of created functions use File | Settings | File Templates.
    }

    override fun attach(view: ProfileContract.View) {
        this.view = view
        context = view.getContexSpecial()
        model = ProfileModel()


    }

    override fun detach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    override fun changePush(on: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}