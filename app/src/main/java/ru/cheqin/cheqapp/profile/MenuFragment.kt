package ru.cheqin.cheqapp.profile

import android.app.Fragment
import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.orhanobut.hawk.Hawk
import io.realm.Realm
import kotlinx.android.synthetic.main.menu_fragment.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.cheqin.cheqapp.BaseView
import ru.cheqin.cheqapp.EmptyActivity
import ru.cheqin.cheqapp.PPActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.main.MainActivity
import ru.cheqin.cheqapp.registration.NewRegistrationActivity
import ru.cheqin.cheqapp.tools.EMAIL
import ru.cheqin.cheqapp.tools.ISREG
import ru.cheqin.cheqapp.tools.NAME
import ru.cheqin.cheqapp.tools.PHONE
import java.util.*

class MenuFragment: Fragment(), KodeinAware, ProfileContract.View {
    override fun getContexSpecial(): Context {

        return this.activity
    }

    fun getHawk(){
        Hawk.init(this.activity).build()
    }

    override val kodein by closestKodein()
    val realm: Realm by instance()

    lateinit var presenter: ProfileContract.Presenter

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        presenter = ProfilePresenter(kodein)
        presenter.attach(this)
        val view = inflater!!.inflate(R.layout.menu_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        getHawk()
        showProfile()

        user_rights.setOnClickListener {
            startActivity<PPActivity>("IS_POLICE" to true)
        }

        private_policy.setOnClickListener {
            startActivity<PPActivity>("IS_POLICE" to false)
        }

        floatingActionButton.setOnClickListener {

            if(!Hawk.contains(IS_PROD)){
                Hawk.put(IS_PROD, false)
            }
            val prod = Hawk.get<Boolean>(IS_PROD)
            if (prod) {

                toast("Подключена тестовая база")
                Hawk.put<Boolean>(IS_PROD, false)
            } else {
                toast("Подключена продакшен база")
                Hawk.put<Boolean>(IS_PROD, true)
            }
        }
    }


    override fun startRegistration() {
        startActivity<NewRegistrationActivity>()
    }

    override fun showProfile() {

        val isReg = Hawk.get<Boolean>(ISREG, false)
        if (isReg) {
            name.text = Hawk.get(NAME)
            phoneNumber.text = Hawk.get(PHONE)
            exit_button.setOnClickListener {
                Hawk.put(ISREG, false)
                Hawk.put(PHONE, UUID.randomUUID().toString())
                Hawk.delete(EMAIL)
                Hawk.put(NAME, "unknown")
                showProfile()
            }

        } else {
            push.visibility = View.GONE
            relativeLayout.visibility = View.GONE
            exit_text.text = "Войти"
            exit_text.setTextColor(Color.parseColor("#417505"))
            exit_button.setOnClickListener {
                startRegistration()
            }
        }
    }

    override fun showConfPolice() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        fun newInstance(): Fragment {
            return MenuFragment()
        }

        val IS_PROD = "IS_PROD"
    }
}