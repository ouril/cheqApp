package ru.cheqin.cheqapp.profile

import android.content.Context
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.Call
import ru.cheqin.cheqapp.BaseView

interface ProfileContract {

    interface Model {
        fun inputNumber(phoneNumber: String, uuid: String, url: String) : Single<Answer>

        fun verefyCode(phoneNumber: String, verificationCode: String, url: String) : Single<Answer>
    }

    interface Presenter {

        fun attach(view: View)

        fun detach()

        fun changePush(on: Boolean)

        fun callSupport(phoneNumber: String, text: String)

    }

    interface View {

        fun getContexSpecial(): Context

        fun startRegistration()

        fun showProfile()

        fun showConfPolice()

    }

}