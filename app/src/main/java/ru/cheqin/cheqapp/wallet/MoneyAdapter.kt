package ru.cheqin.cheqapp.wallet

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.money_item.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import ru.cheqin.cheqapp.MoneyActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.loadImageIntoItem
import java.lang.Exception

class MoneyAdapter internal constructor(val dataSet: JsonArray, val isOfline: Boolean = false) : androidx.recyclerview.widget.RecyclerView.Adapter<MoneyAdapter.CustomViewHolder>() {

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val obj = dataSet.get(position).asJsonObject
        val width = holder.itemView.context.resources.displayMetrics.widthPixels * 91 / 100

        holder.scoreCurrency.text = (obj["bon_val"].asString.toLong() / 100).toString()
        loadImageIntoItem(obj["bon_name"].asString, holder.imageCurrency, width, isOfline)
        // TODO added intent for detail
        holder.itemView.setOnClickListener{
            try {
                holder.itemView.context.startActivity<MoneyActivity>(
                        "pic" to obj["bon_name"].asString,
                        "name" to obj["bon_str_name"].asString,
                        "sum" to obj["bon_val"].asString
                )
            } catch (e:Exception) {
                holder.itemView.context.toast("Нет ключа необходимого в JSON")
            }


        }
    }

    override fun getItemCount(): Int {
        return dataSet.size()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CustomViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.money_item, parent, false)

        return CustomViewHolder(v)
    }

    class CustomViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v), View.OnClickListener {

        val scoreCurrency = v.walleyScore!!
        val imageCurrency = v.imageWalley!!

        override fun onClick(v: View) {}
    }
}

