package ru.cheqin.cheqapp.wallet

import android.annotation.SuppressLint
import android.app.Fragment
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.get
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.orhanobut.hawk.Hawk
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.walley_fragment.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.startActivity
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.cheqin.cheqapp.CardActivity
import ru.cheqin.cheqapp.EmptyActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.scaner.ScannerActivity.Companion.SCAN
import ru.cheqin.cheqapp.dataBaseUtils.CurrencyModel
import ru.cheqin.cheqapp.dataBaseUtils.GiftModel
import ru.cheqin.cheqapp.fragments.AbsResyclerFragment
import ru.cheqin.cheqapp.tools.*
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.net.URL

class WalleyFragment: AbsResyclerFragment(), KodeinAware {

    override val kodein by closestKodein()

    val realm: Realm by instance()

    lateinit var moneyAdapter: MoneyAdapter
    lateinit var giftAdapter: GiftAdapter
    lateinit var phoneId: String
    lateinit var viewContext: View
    var page: Int = 0

    val FULL_PRICE = "FULL_PRICE"
    val FULL_BONUS = "FULL_BONUS"
    val FULL_SUM = "FULL_SUM"
/*
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        page = arguments.getInt("PAGE", 0)
    }
*/
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Hawk.init(this.activity).build()
        url = SERVER//if (Hawk.get("IS_PROD")) SERVER else SERVERD

        val view = inflater!!.inflate(R.layout.walley_fragment, container, false)
        viewContext = view
        return view
    }



    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        qrCard.setOnClickListener {
            startActivity<CardActivity>(
                    SCAN to emailBuilder(phoneId)
            )
        }
        Hawk.init(this.activity).build()
        val mocPhone = Hawk.get<String>(PHONE)
        phoneId = mocPhone


        if (!(Hawk.contains(FULL_SUM) && Hawk.contains(FULL_BONUS))){
            Hawk.put(FULL_BONUS, 0)
            Hawk.put(FULL_SUM, 0)
        }
        val data = realm.where(CurrencyModel::class.java).findAll()
        createOflineResycler(data, viewContext)
        getCachOfListSizeLocal()

        postRxToServer(jsonListBuilder(CHEQ_HISTORY, mocPhone))!!.subscribe { pair, _ ->
            val (response, result) = pair
            if (response.statusCode in 1..399) {
                val json = result.component1().toString()
                val gson = Gson()
                val res = gson.fromJson<JsonObject>(json)

                if (!res.has("error")) {
                    val dataArray = res["result"][0]["bon_val"].asJsonArray
                    val fullBonus = res["result"][0]["bonus_sum"].asInt / 100
                    val fullSumma = res["result"][0]["cheq_main_bonus"].asInt / 100

                    var fullSumBonus : Int = 0

                    dataArray.forEach {
                        fullSumBonus += it["bon_val"].asString.toInt()
                    }

                    Hawk.init(this.activity).build()
                    Hawk.put<Int>(FULL_PRICE, fullSumBonus / 100)
                    Hawk.put<Int>(FULL_BONUS, fullBonus)




                    createRecycler(viewContext, dataArray, R.id.moneyList)
                    val idList = data.map { it.id }

                    val deleteList = idList.filter {
                        it !in dataArray.map {
                            it["bon_name"].asString
                        }
                    }
                    deleteInBase(deleteList)
                    dataArray.forEach {

                        val itId = it["bon_name"].asString
                        val fileSplitParts: List<String> = itId.split('/')
                        val nameFile = fileSplitParts[fileSplitParts.size - 1]
                        val filePath = "${activity.filesDir.absolutePath}/$nameFile"

                        realm.beginTransaction()

                        val currancy = if (itId !in idList) realm.createObject(CurrencyModel::class.java, itId)
                        else data.filter { it.id == itId }[0]
                        currancy.score = it["bon_val"].asString
                        currancy.url = filePath

                        realm.commitTransaction()

                        GlobalScope.launch {
                            douwnLoadPicture(itId, filePath)
                        }
                    }
                    // TODO Вытащить этот баг!!!

                    try {
                        fullSum.text = fullBonus.toString()
                        cheqScore.text = (fullSumBonus / 100).toString()
                    } catch (e: NullPointerException) {
                        e.fillInStackTrace()
                    }
                } else {
                    createOflineResycler(data, viewContext)
                }
            } else {
                createOflineResycler(data, viewContext)
            }
        }
        postRxToServer(jsonListBuilder(GIFT_LIST, mocPhone))!!.subscribe { pair, _ ->

            val (response, result) = pair
            val dataGift = realm.where(GiftModel::class.java).findAll()

            if (response.statusCode in 1..399) {
                val json = result.component1().toString()
                val gson = Gson()
                val res = gson.fromJson<JsonObject>(json)
                if (!res.has("error")) {
                    val dataArray = res["result"][0].asJsonArray
                    createGiftRecycler(viewContext, dataArray, R.id.recyclerGifyView, false)

                    Log.d("REALM GIFT LIST", dataArray.toString())

                    val idList = data.map { it.id }
                    val deleteList = idList.filter { it !in dataArray.map { it[0].asString } }

                    deleteInGiftBase(deleteList)
                    dataArray.forEach {

                        val itId = it[0].asString
                        val fileSplitParts: List<String> = it[3].asString.split('/')
                        val nameFile = fileSplitParts[fileSplitParts.size - 1]
                        val filePath = "${activity.filesDir.absolutePath}/$nameFile"

                        realm.beginTransaction()

                         if (itId !in idList){
                            try {
                                val gift = realm.createObject(GiftModel::class.java, itId)

                                gift.action_id = it[1].asInt
                                gift.name = it[2].asString
                                gift.url = filePath
                            } catch (e: io.realm.exceptions.RealmPrimaryKeyConstraintException) {
                                e.printStackTrace()
                                //realm.where(GiftModel::class.java).equalTo("id", itId)
                            }
                        }
                        else dataGift.filter { it.id == itId }[0]

                        realm.commitTransaction()

                        GlobalScope.launch {
                            douwnLoadPicture(it[3].asString, filePath)
                        }
                    }
                } else {
                    createOflineResyclerGift(dataGift, viewContext)
                    Hawk.init(this.activity).build()
                    getCachOfListSizeLocal()
                }

            }
        }




        giftIcon.setOnClickListener {

            balIcon.setBackgroundResource(R.drawable.buttom_shape_white_left)
            //balIcon.setTextColor(Color.parseColor("#3f97ce"))
            giftIcon.setBackgroundResource(R.drawable.buttom_shape_ritht)
            //giftIcon.setTextColor(Color.parseColor("#ffffff"))
            cardWithBal.visibility = View.GONE
            recyclerGifyView.visibility = View.VISIBLE
            moneyList.visibility = View.GONE

            balIcon.setOnClickListener {
                balIcon.setBackgroundResource(R.drawable.buttom_shape)
                //balIcon.setTextColor(Color.parseColor("#ffffff"))
                giftIcon.setBackgroundResource(R.drawable.buttom_shape_white)
                //giftIcon.setTextColor(Color.parseColor("#3f97ce"))
                cardWithBal.visibility = View.VISIBLE
                recyclerGifyView.visibility = View.GONE
                moneyList.visibility = View.VISIBLE
            }
        }
    }

    fun getCachOfListSizeLocal() {
        val fullSummaLocalBase = Hawk.get(FULL_BONUS, 0)
        if (fullSummaLocalBase > 0) fullSum.setText(fullSummaLocalBase.toString())

        val cheqBase = Hawk.get(FULL_PRICE, 0)

        if (cheqBase > 0) cheqScore.setText(cheqBase.toString())
    }


    private fun deleteInBase(deleteList: List<String>) {
        deleteList.forEach {
            val objForDel = realm.where(CurrencyModel::class.java).equalTo("id", it).findFirst()
            realm.beginTransaction()
            objForDel!!.deleteFromRealm()
            realm.commitTransaction()
        }
    }

    private fun deleteInGiftBase(deleteList: List<String>) {
        deleteList.forEach {
            val objForDel = realm.where(GiftModel::class.java).equalTo("id", it).findFirst()
            realm.beginTransaction()
            if (objForDel != null) {
                objForDel!!.deleteFromRealm()
                    }

            realm.commitTransaction()
        }
    }

    private fun douwnLoadPicture(itId: String?, filePath: String) {
        try {
            val url = URL(itId)
            val urlConection = url.openConnection()
            val bitmap = BitmapFactory.decodeStream(urlConection.getInputStream())
            val f = File(filePath)
            val fOut = FileOutputStream(f)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut.flush()
            fOut.close()

        } catch (e: Exception) {
            Log.d("DOWNLOAD_ERROR", e.toString())
        }
    }

    override fun createRecycler(view: View, data: JsonArray, resyclerId: Int, isOfline: Boolean) {
        //try {
            layoutManagerRecycler = androidx.recyclerview.widget.LinearLayoutManager(view.context) as androidx.recyclerview.widget.RecyclerView.LayoutManager
            moneyAdapter = MoneyAdapter(data, isOfline)
            view.findViewById<androidx.recyclerview.widget.RecyclerView>(resyclerId).apply {
                setHasFixedSize(true)
                layoutManager = layoutManagerRecycler
                adapter = moneyAdapter
            }

    }

    fun createGiftRecycler(view: View, data: JsonArray, resyclerId: Int, isOfline: Boolean) {
            view.findViewById<androidx.recyclerview.widget.RecyclerView>(resyclerId).apply {
                setHasFixedSize(true)
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(view.context)
                adapter = GiftAdapter(data, isOfline)
            }
    }


    private fun createOflineResycler(data: RealmResults<CurrencyModel>, view: View) {
        val jsonData = jsonArray()
        data.forEach {
            jsonData.add(
                    jsonObject(
                            "bon_name" to it.url,
                            "bon_val" to it.score
                    )
            )
        }
        createRecycler(view, jsonData, R.id.moneyList, true)
    }

    private fun createOflineResyclerGift(data: RealmResults<GiftModel>, view: View) {
        val jsonData = jsonArray()
        data.forEach {
            jsonData.add(
                    jsonArray(
                            it.id,
                            it.action_id,
                            it.name,
                            it.url
                    )
            )

        }
        createGiftRecycler(view, jsonData, R.id.recyclerGifyView, true)
    }

    override fun postRxToServer(data: JsonObject): Single<Pair<Response, Result<String, FuelError>>>? {
        Log.d("JSON__DATA", data.toString())
        return url.httpPost().body(data.toString()).rx_responseString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
    }


    companion object {

        fun newInstance(): Fragment {//page: Int): Fragment {
            val wallet = WalleyFragment()
            //val args = Bundle()
            //args.putInt("PAGE", page)
            //wallet.arguments = args
            return wallet
        }
    }
}