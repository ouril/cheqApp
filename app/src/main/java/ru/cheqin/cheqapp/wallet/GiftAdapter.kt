package ru.cheqin.cheqapp.wallet

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.gift_item.view.*
import org.jetbrains.anko.startActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.loadImageIntoItem
import java.lang.Exception

class GiftAdapter internal constructor(
        val dataSet: JsonArray,
        val isOfline: Boolean = false
) : androidx.recyclerview.widget.RecyclerView.Adapter<GiftAdapter.CustomViewHolder>() {

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val obj = dataSet.get(position).asJsonArray
        val width = holder.itemView.context.resources.displayMetrics.widthPixels * 91 / 100

        holder.giftName.text = obj[2].asString
        loadImageIntoItem(obj[3].asString, holder.imageGift, width, isOfline)

        println("------------------------->     $obj")
        if (obj.size() > 6) {
            holder.itemView.setOnClickListener {
                holder.itemView.context.startActivity<GiftActivity>(
                        "pic" to obj[3].asString,
                        "id" to obj[1].asString,
                        "name" to obj[2].asString,
                        "desc" to obj[6].asString,
                        "code" to obj[0].asString,
                        "gift_code" to obj[7].asString,
                        "isSecret" to obj[8].asBoolean
                )

            }
        } else {
            holder.itemView.setOnClickListener {
                holder.itemView.context.startActivity<GiftActivity>(
                        "pic" to obj[3].asString,
                        "id" to obj[1].asString,
                        "name" to obj[2].asString,
                        "desc" to obj[5].asString,
                        "code" to obj[0].asString,
                        "gift_code" to "",
                        "isSecret" to true
                )

            }
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CustomViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.gift_item, parent, false)

        return CustomViewHolder(v)
    }

    class CustomViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v), View.OnClickListener {

        val giftName = v.nameGift!!
        val imageGift = v.gift_image_big!!

        override fun onClick(v: View) {}
    }
}

