package ru.cheqin.cheqapp.wallet

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_gift.*
import org.jetbrains.anko.startActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.actions.OneActionActivity
import ru.cheqin.cheqapp.tools.ID
import ru.cheqin.cheqapp.tools.buildQrBitmap
import ru.cheqin.cheqapp.tools.loadImage
import java.lang.Exception

class GiftActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gift)
        val name = intent.extras["name"] as String
        val id = intent.extras["id"] as String
        val pic = intent.extras["pic"] as String
        val desc = intent.extras["desc"] as String



        try {

            val code = intent.extras["code"] as String
            val gift_code_data = intent.extras["gift_code"] as String
            val isSecret = intent.extras["isSecret"] as Boolean

            if (!isSecret) {
                gift_code.text = gift_code_data
                qrGift.visibility = View.GONE
            } else {
                gift_promo_title.visibility = View.GONE
                gift_code.visibility = View.GONE
                qrGift.setImageBitmap(buildQrBitmap(code))
            }
        } catch (e:Exception) {
            gift_code.visibility = View.GONE
            gift_promo_title.visibility = View.GONE
            e.printStackTrace()
        }
        cancel.setOnClickListener {
            finish()
        }
        loadImage(pic, gift_pic)
        gift_name.text = name
        gift_discription.text = desc
        action_link.setOnClickListener {
            startActivity<OneActionActivity>(
                    ID to id
            )
        }

    }
}
