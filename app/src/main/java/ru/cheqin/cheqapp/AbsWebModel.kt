package ru.cheqin.cheqapp

import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject

open class AbsWebModel {

    private fun getStringSumAndCheqItems(res: JsonObject): Pair<JsonArray, String> {
        var cheq: JsonObject
        if (res.has(RESULT_JSON)) {
            cheq = try {
                res[RESULT_JSON]
                        .asJsonArray[0]
                        .asJsonObject["document"]
                        .asJsonObject["receipt"]
                        .asJsonObject
            } catch (e: java.lang.IllegalStateException) {
                res["document"]
                        .asJsonObject["receipt"]
                        .asJsonObject
            }
        } else {
            cheq = res["document"]
                    .asJsonObject["receipt"]
                    .asJsonObject
        }
        val cheqItems = cheq["items"].asJsonArray
        val sum = cheq["totalSum"].asDouble
        val sumString = "${"%.2f".format(sum / 100).replace(',', '.')}\u20BD"

        return Pair(cheqItems, sumString)
    }


    fun getJsonObjOfResult(result: Result<String, FuelError>): JsonObject {

        Log.d(CHEQ_JSON_LOG, result.component1().toString())

        val json = result.component1().toString()
        val gson = Gson()
        val res = gson.fromJson<JsonObject>(json)
        return res
    }

    private fun rightDate(str: String): String{
        val res:String


        if (str.length > 14) {
            res = "${str.substring(0, 4)}-${str.substring(4, 6)}-${str.substring(6, 8)}${str.substring(8, 11)}:" +
                    "${str.substring(11, 13)}:${str.substring(13, 15)}"
        } else {
            res = "${str.substring(0, 4)}-${str.substring(4, 6)}-${str.substring(6, 8)}${str.substring(8, 11)}:" +
                    "${str.substring(11, 13)}"
        }
        return res
    }

    fun scanMaping(st: String): Map<String, String> {
        val str = st.replace("\"", "")
        val dict: HashMap<String, String> = HashMap()

        val res: List<String> = str.split("&")
        for (i in res){
            dict.put(i.split("=")[0], i.split("=")[1])
        }
        val urlAdress = "https://proverkacheka.nalog.ru:9999/v1/inns/*/kkts/*/fss/" +
                "${dict.get("fn")}/tickets/${dict.get("i")}?fiscalSign=${dict.get("fp")}&sendToEmail=no"

        val summa = dict.get("s").toString().replace(".", "")

        val dateOf = rightDate(dict.get("t").toString())

        val urlAdresstTry = "https://proverkacheka.nalog.ru:9999/v1/ofds/*/inns/*/fss/" +
                "${dict.get("fn")}/operations/1/tickets/${dict.get("i")}?fiscalSign=${dict.get("fp")}" +
                "&date=${dateOf}&sum=${summa}"

        return mapOf<String, String>(
                URL_TRY to urlAdresstTry,
                URL_CHEQ to urlAdress,
                SUM to summa,
                DATE to dateOf
        )
    }

    companion object {
        const val CHEQ_JSON_LOG = "CHEQ_JSON_LOG"
        const val BILL_GIFT = "bill_gift"
        const val ERRORS_KEY = "error"
        const val RESULT_JSON = "result"
        const val IM_OWNER = "im_owner"
        const val URL_TRY = "urlTry"
        const val URL_CHEQ ="url"
        const val SUM = "sum"
        const val DATE = "date"
        const val BILL_BON_VAL = "bill_bon_val"
        const val MAIN_BONUS = "cheq_main_bonus"
        const val LOCATION = "LOCATION"
    }
}