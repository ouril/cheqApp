package ru.cheqin.cheqapp

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
//import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.jsonObject

// import com.jakewharton.rxbinding2.widget.RxTextView
// import com.orhanobut.hawk.Hawk
// import io.reactivex.Single
// import io.reactivex.android.schedulers.AndroidSchedulers
// import io.reactivex.schedulers.Schedulers
// import kotlinx.android.synthetic.ru.cheqin.cheqapp.main.activity_registration.*
// import org.jetbrains.anko.design.snackbar
// import org.jetbrains.anko.startActivity
// import ru.cheqin.cheqapp.tools.*
// import io.realm.log.RealmLog.debug
// import org.jetbrains.anko.toast
//
//
// class RegistrationActivity: AppCompatActivity() {
// override fun onCreate(savedInstanceState: Bundle?) {
// super.onCreate(savedInstanceState)
// setContentView(R.layout.activity_registration)
// Hawk.init(this).build()
//
// try {
// if (intent.extras.containsKey("ISFAIL")) {
// if (intent.extras.getBoolean("ISFAIL", false)) {
// snackbar(registration, "Проверьте правильность введенных данных!")
// }
// }
// } catch (e: Exception) {
// debug(e.toString())
// }
//
// if (Hawk.contains(NAME) && Hawk.contains(PHONE)){
// finish()
// }
//
// imageName.setImageResource(R.drawable.ic_user_name_reg_white_fill)
//
// imagePhone.setImageResource(R.drawable.ic_phone_sign_disabled_white)
//
// val nameCache: String
// if  (Hawk.contains(NAME_CACHE)) {
// nameCache = Hawk.get(NAME_CACHE)
// editName.setText(nameCache)
// }
//
// val phoneCache: String
// if (Hawk.contains(PHONE_CACHE)) {
// phoneCache = Hawk.get(PHONE_CACHE)
//
// editPhone.setText(phoneCache)
// }
//
// setFocusOnField()
// editName.requestFocus()
// editName.filters = arrayOf(RuTextFilter())
//
// RxTextView.textChanges(editName)
// .subscribe {
// if (it.length > 1){
// imageName.setImageResource(R.drawable.ic_return_sms_code)
// imageName.setOnClickListener {
// changeField()
// Hawk.delete(NAME_CACHE)
// Hawk.put(NAME_CACHE, editName.text.toString())
// }
// editName.setOnKeyListener { v, keyCode, event ->
// if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
// changeField()
// Hawk.delete(NAME_CACHE)
// Hawk.put(NAME_CACHE, editName.text.toString())
// return@setOnKeyListener true
// }
// false
// }
//
//
//
// }
// else {
// imageName.setImageResource(R.drawable.ic_user_name_reg_white_fill)
// }
//
// }
//
// RxTextView.textChanges(editPhone)
// .subscribe {
// if (editPhone.text.length < 3){
// editPhone.setText("+7(")
// editPhone.setSelection(3)
// }
//
// if(phoneVaslidator(it.toString())) {
//
// imagePhone.setImageResource(R.drawable.ic_return_sms_code)
// imagePhone.setOnClickListener {
// reactToPhoneEdit()
// }
// editPhone.setOnKeyListener { v, keyCode, event ->
// if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
// reactToPhoneEdit()
// return@setOnKeyListener true
// }
// false
// }
// }
// if (editPhone.text[0] == '8') {
// editPhone.setText("+7${it.substring(1, it.length - 1)}")
// editPhone.setSelection(editPhone.text.length-1)
//
// }
//
// setterRightSymbol(it, 2, "(")
//
// setterRightSymbol(it, 6, ")")
//
// setterRightSymbol(it, 10, "-")
//
//
// }
//
// theButton.setOnClickListener {
//
// nextStepRegistration()
// }
//
// RxTextView.textChanges(editCode).subscribe {
//
// if (editCode.text.length == 4 && '-' !in  it){
// val codeWithDefise = "${editCode.text.subSequence(0, 3)} - ${editCode.text[3]}"
// editCode.setText(codeWithDefise)
// editCode.setSelection(7)
// }
//
// if (it.length == 9) {
// val phone = editPhone.text.toString()
// .replace("(","")
// .replace(")","")
// .replace("-", "")
// val name = editName.text.toString()
// val pass = editCode.text.toString()
// .replace(" ", "")
// .replace("-", "")
// imageViewCode.setImageResource(R.drawable.ic_return_sms_code)
// imageViewCode.setOnClickListener {
// finishRegistration(phone, pass, name)
// }
//
// editCode.setOnKeyListener { v, keyCode, event ->
// if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
// finishRegistration(phone, pass, name)
// return@setOnKeyListener true
// }
// false
// }
// }
// }
//
// privatPolicy.setOnClickListener {
// startActivity<DocumentActivity>()
// }
// }
//
// fun nextStepRegistration() {
// theButton.isEnabled = false
// theButton.setCardBackgroundColor(Color.parseColor("#48d2a0"))
// buttonText.text = "ДАЛЕЕ"
// privatPolicy.visibility = View.VISIBLE
// fieldPhoneInstruction.visibility = View.GONE
// fieldPhoneInvintation.visibility = View.GONE
// editName.visibility = View.VISIBLE
// editName.textAlignment = View.TEXT_ALIGNMENT_CENTER
// editName.alpha = 1.0f
// editName.textSize = 30f
// theButton.isEnabled = true
// theButton.setOnClickListener {
// /**
// * There's we try to login in nalog.proverkachekov.ru
// */
// registation()
//
// }
// }
//
// fun registation() {
// privatPolicy.visibility = View.GONE
// val normalisedPhone = editPhone.text.toString()
// .replace("(","")
// .replace(")","")
// .replace("-","")
// sendRegistration(
// normalisedPhone,
// emailBuilder(normalisedPhone),
// editName.text.toString()
// )!!.subscribe { pair, _ ->
// val (response, result) = pair
// when (result) {
// is Result.Success -> {
// toast("Регистрация прошла успешно, введите пароль из SMS")
// }
// is Result.Failure -> {
// sendHandleandleResetRequest()
//
// }
// }
// }
// }
//
// fun setterRightSymbol(s: CharSequence, position: Int, symbol: String) {
// if (s.length == (position + 1) && !(symbol in s)) {
// val codeWithBrakets = "${s.substring(0, position)}$symbol${s[position]}"
// editPhone.setText(codeWithBrakets)
// editPhone.setSelection(position + 2)
// }
// }
//
// fun reactToPhoneEdit() {
// setFocusOnField(true, false)
// editName.visibility = View.GONE
// imagePhone.setImageResource(R.drawable.ic_return_sms_code)
// imageName.visibility = View.GONE
// fieldNameInvitation.visibility = View.GONE
// fieldNameInstruction.visibility = View.GONE
// fieldPhoneInvintation.alpha = 1.0f
// fieldPhoneInstruction.text = "нажмите на номер для редактирования"
// fieldPhoneInvintation.text = "Проверьте правильность ввода номера"
// imagePhone.visibility = View.GONE
// editPhone.textAlignment = View.TEXT_ALIGNMENT_CENTER
// editPhone.alpha = 1.0f
// editPhone.textSize = 30f
// Hawk.delete(PHONE_CACHE)
// Hawk.put(PHONE_CACHE, editPhone.text.toString())
// theButton.visibility = View.VISIBLE
// }
//
// private fun sendHandleandleResetRequest() {
// val phone = editPhone.text.toString()
// .replace("(","")
// .replace(")","")
// .replace("-", "")
// sendResetRequest(phone)!!.subscribe { anotherPair, _ ->
// val (responseReset, resultReset) = anotherPair
// when (resultReset) {
// is Result.Success -> {
// println("responseCode ------> ${responseReset.statusCode}")
// println("responseMessage ----->${responseReset.responseMessage}")
// val name = editName.text.toString()
//
// editName.visibility = View.GONE
// editPhone.visibility = View.GONE
// editCode.visibility = View.VISIBLE
// imageViewCode.visibility = View.VISIBLE
//
// buttonText.text = "УЧАСТВОВАТЬ В АКЦИЯХ"
// theButton.setOnClickListener {
// val pass = editCode.text.toString()
// .replace(" ", "")
// .replace("-", "")
// finishRegistration(phone, pass, name)
// }
// }
// is Result.Failure -> {
// println("responseCode ------> ${responseReset.statusCode}")
// println("responseMessage ----->${responseReset.responseMessage}")
// finish()
// toast("Проверьте соединеие с интернетом")
// }
// }
// }
// }
//
// fun finishRegistration(phone: String, pass: String, name: String, isReg: Boolean=true) {
// sendLoginRequest(phone, pass)!!.subscribe { loginPair, _ ->
// val (responseLogin, resultLogin) = loginPair
// when (resultLogin) {
// is Result.Success -> {
// Hawk.put(PASSWORD, pass)
// Hawk.put(NAME, name)
// Hawk.put(PHONE, phone)
// if (isReg) finish()
// }
// is Result.Failure -> {
// toast("неправильный пароль, телефон или имя")
// finish()
// startActivity<RegistrationActivity>("ISFAIL" to true)
// }
// }
// }
// }
//
// private fun changeField() {
// setFocusOnField(false)
// imageName.setImageResource(R.drawable.ic_sucsess_check_white_light)
// editPhone.requestFocus()
//
//
// }
//
// private fun setFocusOnField(name: Boolean = true, notAllOk: Boolean = true) {
// val vis: Float
// val inVis: Float
//
// if (name && notAllOk) {
//
// vis = 1.0f
// inVis = 0.4f
// imageName.setImageResource(R.drawable.ic_user_name_reg_white_fill)
// imagePhone.setImageResource(R.drawable.ic_phone_sign_disabled_white)
// editName.isEnabled = true
// editPhone.isEnabled = false
//
// } else if (!name && notAllOk){
//
// vis = 0.4f
// inVis = 1.0f
// imageName.setImageResource(R.drawable.ic_user_name_reg_inactive_white_fill)
// if (editPhone.text.isNullOrBlank()) {
// imagePhone.setImageResource(R.drawable.ic_phone_sign_white)
// } else {
// imagePhone.setImageResource(R.drawable.ic_return_sms_code)
// }
// editName.isEnabled = false
// editPhone.isEnabled = true
// } else {
// vis = 0.4f
// inVis = 0.4f
// editName.isEnabled = false
// editPhone.isEnabled = false
// }
//
// editName.alpha = vis
// editPhone.alpha = inVis
//
// fieldNameInvitation.alpha = vis
// fieldNameInstruction.alpha = vis
//
// fieldPhoneInstruction.alpha = inVis
// fieldPhoneInvintation.alpha = inVis
//
// }
//
//
// fun sendResetRequest(phone: String): Single<Pair<Response, Result<String, FuelError>>>? {
//
// val json = jsonObject(
// "phone" to phone
// )
//
// return PROVERKA_RESET.httpPost()
// .body(json.toString())
// .header(headersForReset())
// .rx_responseString()
// .observeOn(AndroidSchedulers.mainThread())
// .subscribeOn(Schedulers.newThread())
// }
//
//
// fun sendRegistration(phone: String, email: String, name: String) : Single<Pair<Response, Result<String, FuelError>>>? {
//
// val json = jsonObject(
// "phone" to phone,
// "email" to email,
// "name" to name
// )
//
// return PROVERKA_SIGN_UP.httpPost()
// .body(json.toString())
// .header(headersForRegistration())
// .rx_responseString()
// .observeOn(AndroidSchedulers.mainThread())
// .subscribeOn(Schedulers.newThread())
// }
//
// fun sendLoginRequest(phone: String, pass: String) : Single<Pair<Response, Result<String, FuelError>>>? {
//
// return PROVERKA_LOGIN.httpGet()
// .authenticate(phone, pass)
// .header(headersForRegistration())
// .rx_responseString()
// .observeOn(AndroidSchedulers.mainThread())
// .subscribeOn(Schedulers.newThread())
// }
//
//
// }