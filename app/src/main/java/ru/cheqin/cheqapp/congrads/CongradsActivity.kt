package ru.cheqin.cheqapp.congrads

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.find_balls_screen.*
import ru.cheqin.cheqapp.cheq.CheqActivity
import org.jetbrains.anko.startActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.scaner.ScannerActivity
import java.text.SimpleDateFormat

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CongradsActivity: AppCompatActivity() {

    companion object {
        val SCAN = "SCAN"
        val IS_GETTED ="IS_GETTED"
        val DATE ="DATE"
        val BONUS ="BONUS"
        val HAS_GIFT = "HAS_GIFT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.find_balls_screen)


        val scan = intent.extras.getString(CongradsActivity.Companion.SCAN, "")
        val isGetted = intent.extras.getBoolean(CongradsActivity.Companion.IS_GETTED, false)
        val date = intent.extras.getString(CongradsActivity.Companion.DATE, "")
        val bonus = intent.extras.getInt(CongradsActivity.Companion.BONUS, 0)
        val hasGift = intent.extras.getString(CongradsActivity.Companion.HAS_GIFT, "")

        if (hasGift.isNullOrBlank()) {
            giftButton.visibility = View.INVISIBLE

        }
        if (isGetted) {
            imageState.setImageResource(R.drawable.ic_success_cheq)
            congrads.text = "Чек получен"
        }
        if (bonus > 0) balls.text = bonus.toString() else bonusBottom.visibility = View.INVISIBLE
        val formatedDate = date.let {
            val(datePart, time) = it.split(' ')
            val formater = SimpleDateFormat("yyyy-MM-dd").parse(datePart)
            "${SimpleDateFormat("dd.MM.yyyy").format(formater)} в $time"

        }
        textView9.setText(formatedDate)
        scanButton.setOnClickListener {
            startActivity<CheqActivity>(ScannerActivity.SCAN to scan, "CALL_FROM_LIST" to true)
            finish()
        }
        scaner.setOnClickListener {
            startActivity<ScannerActivity>()
            finish()
        }




    }

}