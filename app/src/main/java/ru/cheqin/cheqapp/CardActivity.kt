package ru.cheqin.cheqapp

import android.app.Activity
import android.os.Bundle
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_card.*
import org.jetbrains.anko.toast
import ru.cheqin.cheqapp.tools.EMAIL
import ru.cheqin.cheqapp.tools.buildQrBitmap
import ru.cheqin.cheqapp.tools.loadImage
import java.lang.Exception

class CardActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)
        Hawk.init(this).build()
        try {
            val mail = Hawk.get<String>(EMAIL)
            qr_pic.setImageBitmap(buildQrBitmap(mail))
            text_qr.text = mail
        } catch (e: Exception){
            DialogView.showDialog(this, true)

        }
        back.setOnClickListener {
            finish()
        }
    }
}
