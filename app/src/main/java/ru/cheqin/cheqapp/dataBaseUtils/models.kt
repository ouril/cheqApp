package ru.cheqin.cheqapp.dataBaseUtils

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

//@RealmClass
open class ActionsModel : RealmObject() {

    @PrimaryKey
    var id: Int = 0
    var name: String? = null
    var start: Int? = null
    var end: Int? = null
    var description: String? = null
    var picture: String? = null
    var bonus: Int? = null
    var slug: String? = null

}

open class BillModel : RealmObject() {

    @PrimaryKey
    var id: String = ""
    var bonus = false
    var sum: String? = null
    var date: Date? = null
    var date_time: String? = null
    var scan_time: Date? = null
    var organization: String? = null
    var icon: String? = null
    var orgName: String? = null
    var shortOrgName: String? = null
    var letters: String? = null
    var isGetted: Boolean = false
    var isDontGetted: Boolean = false
}

open class CurrencyModel : RealmObject() {

    @PrimaryKey
    var id: String = ""
    var score: String? = null
    var url: String? = null
}

open class BonusModel : RealmObject(){

    var currency: CurrencyModel? = null
    var bill: BillModel? = null
    var value: Int = 0
}

open class CheqItem : RealmObject(){
    var bill: BillModel? = null
    var price: Int? = 0
    var count: Int? = 0
    var name: String? = null
}

open class GiftModel : RealmObject() {

    @PrimaryKey
    var id: String = ""
    var action_id: Int? = null
    var name: String? = null
    var url: String? = null

}