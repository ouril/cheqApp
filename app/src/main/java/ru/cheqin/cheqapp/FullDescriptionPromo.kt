package ru.cheqin.cheqapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_full_description_promo.*
import ru.cheqin.cheqapp.tools.ID
import ru.cheqin.cheqapp.tools.SERVER



class FullDescriptionPromo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_description_promo)
        val id_promo = intent.extras[ID].toString().toInt()!!

        cancel.setOnClickListener {
            finish()
        }

        SERVER.httpPost().body(
                jsonObject(
                        "method" to "bill_api.get_law_info",
                        "params" to jsonArray(id_promo),
                        "id" to 1
               ).toString()
        ).responseString { request, response, result ->
            when(result){

                is Result.Success -> {
                    val gson = Gson()
                    val res = gson.fromJson<JsonObject>(result.component1()!!)
                    if (res["result"].asJsonArray[0].asString != "")
                        promo_text.setHtml(res["result"].asJsonArray[0].asString)
                }
                is Result.Failure -> {}
            }
        }
    }
}
