package ru.cheqin.cheqapp.tools

import android.provider.ContactsContract


/**
 * LINKS
 */
const val SERVER = "https://kernel.cheqin.ru/tnt"
const val SERVERD ="https://kernel.dev.cheqin.ru/tnt"
const val SERVER_RET ="https://kernel.cheqin.ru/"
const val SERVER_DEV = "https://kernel.dev.cheqin.ru/"
const val PROVERKA = "proverkacheka.nalog.ru:9999"
const val SPECIAL_LINK = "https://app.cheqin.ru/$"
const val PROVERKA_LOGIN = "https://proverkacheka.nalog.ru:9999/v1/mobile/users/login"
const val PROVERKA_RESET = "https://proverkacheka.nalog.ru:9999/v1/mobile/users/restore"
const val PROVERKA_SIGN_UP = "https://proverkacheka.nalog.ru:9999/v1/mobile/users/signup"

/**
 * SPECIAL WORDS
 */
const val NONE = "NONE"
const val BARCODE = "BARCODE"
const val ISREG = "ISREG"
const val EMAIL = "EMAIL"
const val PHONE = "PHONE"
const val NAME = "NAME"
const val REQUEST_ERR = "REQUEST_ERR"
const val PASSWORD = "password"
const val GEOLOC = "geolocation"
const val LATITUDE = "latitude"
const val LONGTITUDE = "longitude"


/**
 * POST METHODS
 */
const val ID = "id"
const val PARAMS = "params"
const val METHOD = "method"

const val BILL_LIST = "list_active_promo"
const val CHEQ_HISTORY = "bill_list"
const val GIFT_LIST = "gifts_list"
const val CHECK_GIFT = "check_gift"
const val PROMO_ID = "get_active_promo_id"
const val NEW_BILL = "new_bill"
const val SCAN_BILL = "get_scanned_bill"

/**
 * HAWK KEYS
 */
const val NAME_CACHE = "NAME_CACHE"
const val PHONE_CACHE = "PHONE_CACHE"
