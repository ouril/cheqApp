package ru.cheqin.cheqapp.tools

import android.text.InputFilter
import android.text.Spanned


class RuTextFilter : InputFilter {
    override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        val regax = Regex("[А-Яа-яёЁ\b]+")
        if (!regax.matches(source.toString())) {
            return ""
        }
        return source
    }
}