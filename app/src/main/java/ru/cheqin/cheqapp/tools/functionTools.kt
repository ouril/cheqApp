package ru.cheqin.cheqapp.tools

import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.github.salomonbrys.kotson.*
import com.google.gson.JsonObject
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import io.reactivex.functions.Function6
import net.glxn.qrgen.android.QRCode
import java.io.File
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*


fun loadImageIntoItem(url: String, view: ImageView, width: Int, isOfline: Boolean = false) {
    val urlForLoad = if (!isOfline) url else "file://" + File(url).absolutePath
    Log.d("WIDTH", width.toString())
    Picasso.get()
            .load(urlForLoad)
      //      .centerInside()
            .resize(width, width*100/130)
            .centerInside()

            .into(view)
}


fun loadImageIntoItemForOneAction(url: String, view: ImageView, width: Int, isOfline: Boolean = false) {
    val urlForLoad = if (!isOfline) url else "file://" + File(url).absolutePath
    Log.d("WIDTH", width.toString())
    Picasso.get()
            .load(urlForLoad)
            //      .centerInside()
            .resize(width, width*100/130)
            .centerInside()

            .into(view)
}

fun loadImage(url: String, view: ImageView) {
    Picasso.get()
            .load(url)
            .into(view)
}



fun jsonListBuilder(method: String, phone: String? = null):JsonObject =
    jsonObject(
        METHOD to "bill_api.$method",
        PARAMS to jsonArray(phone),
            ID to 1
)

fun jsonBuilderWithGeo(
        userId:String,
        qrData:String,
       // jsonBill:JsonObject,
        geoloc:Boolean=false,
        latitude:String="",
        longitude:String=""
): JsonObject =
        jsonObject(
                METHOD to "bill_api.get_scanned_bill",
                PARAMS to jsonArray(
                        userId,
                        qrData,
                        //jsonBill,
                        jsonObject(
                                GEOLOC to geoloc,
                                LATITUDE to latitude,
                                LONGTITUDE to longitude
                        ),
                        System.currentTimeMillis() / 1000L
                ),
                ID to 1
        )


fun emailBuilder(phone: String): String =
        "${phone.substring(1)}@cmail.cheqin.ru"



fun buildQrBitmap(qr: String): Bitmap =
       QRCode.from(qr).bitmap()


fun String.phoneStringStrip(): String {
    if (phoneVaslidator(this)) {
        return this.replace("(","")
                .replace(")","")
                .replace("-", "")
    }
    return this
}



