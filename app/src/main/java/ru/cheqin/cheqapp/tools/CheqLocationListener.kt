package ru.cheqin.cheqapp.tools

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat

class CheqLocationListener : LocationListener {

    override fun onLocationChanged(loc: Location) {
        imHere = loc
    }

    override fun onProviderDisabled(provider: String) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

    companion object {

        var imHere: Location? = null

        fun setUpLocationListener(context: Context)
        {
            val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            val locationListener = CheqLocationListener()
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                println("-------------------------> ошибка прав")
                imHere = null
                return
            }

            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    0,
                    0f,
                    locationListener) // здесь можно указать другие более подходящие вам параметры

            imHere = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)

            println("--------------------------->  $imHere")

        }
    }
}