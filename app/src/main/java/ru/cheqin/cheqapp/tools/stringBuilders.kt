package ru.cheqin.cheqapp.tools

import android.annotation.SuppressLint
import android.provider.Settings
import java.lang.Long
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

/**
 * HEADERS BUILDER
 */
fun headers(): HashMap<String,String> {
    val headersForSend = hashMapOf<String, String>()
    headersForSend["Host"] = PROVERKA
    headersForSend["Version"] = 2.toString()
    headersForSend["ClientVersion"] = "1.4.4.1"
    headersForSend["Connection"] = "Keep-Alive"
    headersForSend["Accept-Encoding"] = "gzip"
    headersForSend["User-Agent"] = "okhttp/3.0.1"
    headersForSend["Device-Id"] = Settings.Secure.ANDROID_ID
    headersForSend["Device-OS"] = "Android ${android.os.Build.VERSION.RELEASE}"
    return headersForSend
}

fun headersForRegistration(): HashMap<String,String> {
    val headersForSend = headers()
    headersForSend["Content-Type"] = "application/json; charset=UTF-8"
    return headersForSend
}

fun headersForReset(): HashMap<String,String> {
    val headersForSend = headersForRegistration()
    headersForSend["Content-Length"] = 24.toString()
    return headersForSend
}



        /**
 * DateTime builder
 */


fun getDateTimeFromUnix(s: String): String {
    val pattern = "dd.MM.yy"
    try {
        val unixTime = Long.parseLong(s)
        val time = Date(unixTime * 1000)
        val sdf = SimpleDateFormat(pattern)
        sdf.timeZone = TimeZone.getTimeZone("GMT+3")
        return sdf.format(time)
    } catch (e: Exception) {
        return e.toString()
    }
}

fun getDateTimeFromUnixWithMonthName(s: String): String {
    val pattern = "dd MMMM"
    try {
        val unixTime = Long.parseLong(s)
        val time = Date(unixTime * 1000)
        val sdf = SimpleDateFormat(pattern)
        sdf.timeZone = TimeZone.getTimeZone("GMT+3")
        return sdf.format(time)
    } catch (e: Exception) {
        return e.toString()
    }
}


/**
 * RegExp or string validators
 */

fun phoneVaslidator(str: String): Boolean =
        Regex("""^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}${'$'}""").matches(str)

fun qrDataValidator(str: String):Boolean {
    val normolizedString = str.trim().filter {
        Regex("""[A-Za-z0-9&=]""").matches(it.toString())
    }
    return Regex(""""?t=([\dT]+)&s=([\d\.]+)&fn=(\d+)&i=(\d+)&fp=(\d+)&n=(\d)(\w+)?"?""")
            .matches(normolizedString)

}

fun linkVaslidator(str: String): Boolean = str.startsWith("http://") || str.startsWith("https://")

fun isSpecialLinkValidator(str: String): Boolean = str.startsWith(SPECIAL_LINK)

fun rightDate(str: String): String{
    val res:String
    if (str.length > 14) {
        res = "${str.substring(0, 4)}-${str.substring(4, 6)}-${str.substring(6, 8)}${str.substring(8, 11)}:" +
                "${str.substring(11, 13)}:${str.substring(13, 15)}"
    } else {
        res = "${str.substring(0, 4)}-${str.substring(4, 6)}-${str.substring(6, 8)}${str.substring(8, 11)}:" +
                "${str.substring(11, 13)}"
    }
    return res
}

fun stringBuild(str: String): HashMap<String, String> {

    val dict: HashMap<String, String> = HashMap()

    val res: List<String> = str.split("&")
    for (i in res){
        dict.put(i.split("=")[0], i.split("=")[1])
    }
    val urlAdress = "https://proverkacheka.nalog.ru:9999/v1/inns/*/kkts/*/fss/" +
            "${dict.get("fn")}/tickets/${dict.get("i")}?fiscalSign=${dict.get("fp")}&sendToEmail=no"

    val summa = dict.get("s").toString().replace(".", "")

    val dateOf = rightDate(dict.get("t").toString())

    val urlAdresstTry = "https://proverkacheka.nalog.ru:9999/v1/ofds/*/inns/*/fss/" +
            "${dict.get("fn")}/operations/1/tickets/${dict.get("i")}?fiscalSign=${dict.get("fp")}" +
            "&date=${dateOf}&sum=${summa}"

    dict.put("url", urlAdress)
    dict.put("urlCheck", urlAdresstTry)
    dict.put("dateOf", dateOf)
    dict.put("summa", summa)

    return dict
}

