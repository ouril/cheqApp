package ru.cheqin.cheqapp.win

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.win_activity.*
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.loadImage
import ru.cheqin.cheqapp.scaner.ScannerActivity


class WinActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.win_activity)
        val isSpec = intent.extras.getBoolean(ScannerActivity.ISSPEC)
        val name = intent.extras.getString(ScannerActivity.PRESENT_NAME)
        val idProd = intent.extras.getString(ScannerActivity.PROD_ID)
        val isWin = intent.extras.getBoolean(ScannerActivity.WIN)
        val pic = intent.extras.getString(ScannerActivity.PRESENT_PIC)
        val prod = intent.extras.getString(ScannerActivity.PRESENT_PROD)

        if (isWin){
            val text = "Ваш приз $name"
            loadImage(pic, imageView15)
            textPresent.setText(text)
        } else {
            imageView15.visibility = View.GONE
            textPresent.text = "Продолжайте сканировать\nкоды и искать призы\n\n Чем больше кодов вы просканируете, тем выше вероятность получить приз"
            mainText.text = "Приз где-то\n рядом"
            imageView5.visibility = View.GONE
            /*
            textPart1.visibility = View.VISIBLE
            textPart2.visibility = View.VISIBLE
            goodLuck.visibility = View.VISIBLE
            imageView12.setImageResource(R.drawable.ic_gift_box_miss)
            imageView13.setImageResource(R.drawable.ic_gift_somewhere)
*/

        }
        //if (isSpec) about_action.visibility = View.GONE

        floatingCancel.setOnClickListener {
            finish()
        }
        /*
        about_action.setOnClickListener {
            startActivity<OneActionActivity>(
                    ID to idProd
            )
        }
        */
    }
}


