package ru.cheqin.cheqapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_pp.*

class PPActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pp)
        val isPolice = intent.extras.getBoolean("IS_POLICE")
        val url = if(isPolice) "https://cheqin.ru/termsofuse.html" else  "https://cheqin.ru/policy.html"
        webView.loadUrl(url)
    }
}
