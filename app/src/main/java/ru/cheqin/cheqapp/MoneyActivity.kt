package ru.cheqin.cheqapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_money.*
import ru.cheqin.cheqapp.actions.OneActionActivity
import ru.cheqin.cheqapp.tools.ID
import ru.cheqin.cheqapp.tools.loadImage
import java.lang.Exception
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import ru.cheqin.cheqapp.tools.PHONE
import ru.cheqin.cheqapp.tools.SERVER

class MoneyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_money)

        val name = intent.extras["name"] as String
        val sum = intent.extras["sum"] as String
//        val aboutData = intent.extras["about"] as String
        val pic = intent.extras["pic"] as String

        Hawk.init(this).build()

        SERVER.httpPost().body(
                jsonObject(
                        "method" to "bill_api.get_bonus_description_user",
                        "params" to jsonArray(Hawk.get<String>(PHONE), pic),
                        "id" to 1
                ).toString()
        ).responseString { request: Request, response: Response, result: Result<String, FuelError> ->

            val gson = Gson()
            val res = gson.fromJson<JsonObject>(result.component1().toString())
            when(result){
                is Result.Success -> {
                    about.text = res["result"].asJsonArray[0].asJsonArray[3].asString
                    org_name.text = res["result"].asJsonArray[0].asJsonArray[0].asString
                    org_name3.text = res["result"].asJsonArray[0].asJsonArray[2].asString
                }
                is Result.Failure -> {
                    toast("Проверьте соединение с интернетом!")
                }
            }
        }

        //aboutData
        money_count.text = "${sum.toInt() / 100}"
        loadImage(pic, money_pic)
        //org_name.text = name

        try {
            val id = intent.extras[ID] as String
            action_link.setOnClickListener {

                startActivity<OneActionActivity>(ID to id)
                finish()
            }
        } catch (e: Exception) {
            action_link.visibility = View.GONE

        }



        cancel.setOnClickListener {
            finish()
        }
    }
}
