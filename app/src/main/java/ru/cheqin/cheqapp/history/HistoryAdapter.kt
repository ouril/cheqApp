package ru.cheqin.cheqapp.history


import android.content.Intent
import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.cheq_hat.view.*
import kotlinx.android.synthetic.main.item_cheq_list.view.*
import ru.cheqin.cheqapp.cheq.CheqActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.cheq.CheqData
import ru.cheqin.cheqapp.scaner.ScannerActivity
import ru.cheqin.cheqapp.cheq.CheqHistoryHat

import ru.cheqin.cheqapp.cheq.CheqView
import ru.cheqin.cheqapp.tools.loadImage
import java.text.SimpleDateFormat


class HistoryAdapter internal constructor(val dataSet: List<Any>) : androidx.recyclerview.widget.RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {


    val ID = "ID"
    val ID_KEY = "id"
    val SUM_KEY = "sum"
    val DATATIME_KEY = "date_time"
    val BONUS = "bonus"




    override fun getItemCount(): Int {
        return dataSet.size
    }


    override fun getItemViewType(position: Int): Int {

        return when (dataSet[position]) {
            is CheqData -> {
                0
            }

            else -> {
                1
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = when(viewType) {
                0 -> LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_cheq_list, parent, false)
                else -> LayoutInflater.from(parent.context)
                            .inflate(R.layout.cheq_hat, parent, false)

        }
        return ViewHolder(v, viewType)


    }


    inner class ViewHolder(v: View, viewType: Int = 0) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v),
            View.OnClickListener {
        val holderType: Int = viewType
        val holderView = v

        override fun onClick(v: View?) {
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(holder.holderType) {
            0 -> {
                val obj = dataSet.get(position) as CheqData
                with(holder.holderView) {
                    if (obj.notFound){
                        mainText.text = "Чек не найден"
                        imageState.setImageResource(R.drawable.ic_not_found)
                    } else {

                        if (obj.isGetted) {
                            mainText.text = "Чек получен"
                            imageState.setImageResource(R.drawable.ic_success_cheq)
                        } else {
                            mainText.text = "Чек в обработке"
                            imageState.setImageResource(R.drawable.ic_group_6)
                        }
                    }

                    txtOptionDigit.text = "${obj.sum} \u20BD"
                    val dt = obj.date_time!!.replace('T', ' ')
                    val (date
                            , time)
                    = dt
                        .replace("\"", "")
                        .split(" ")
                    val dateForString = SimpleDateFormat("yyyy-MM-dd").parse(date)
                    val resultDateFrString =  SimpleDateFormat("dd MMMM yyyy").format(dateForString)
                    textTime.text = "Отсканировано $resultDateFrString года"


                    if(obj.organization != null && obj.organization != ""){
                        linearLayoutCompat.visibility = View.VISIBLE
                        organizationLine.visibility = View.VISIBLE
                        organizationPicButton.visibility = View.GONE
                        organizationPic.visibility =  View.GONE
                        obj.iconOrg?.let {

                            if (it.isNotBlank() && it.isNotEmpty()) {
                                val s = obj.iconOrg!!
                                loadImage(s, organizationPic)
                                organizationPic.visibility = View.VISIBLE
                            }
                            else {

                                organizationPicButton.text = obj.short_name
                                organizationPicButton.visibility = View.VISIBLE//organizationName
                            }
                        }
                        organizationName.text = obj.organization
                    } else {
                        linearLayoutCompat.visibility = View.GONE
                        organizationLine.visibility = View.GONE
                    }



                    /**
                     * android.support.v7.widget.LinearLayoutCompat
                    android:id="@+id/linearLayoutCompat"
                    android:visibility="gone"
                    android:layout_width="match_parent"
                    android:orientation="horizontal"
                    android:gravity="center"
                    android:layout_height="36dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="1.0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/constraintLayout">

                    <ImageView
                    android:id="@+id/organizationPic"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"

                    app:srcCompat="@drawable/ic_chevron"
                    android:layout_weight="1"
                    />

                    <TextView
                    android:id="@+id/organizationName"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_weight="6"
                    android:text="ООО Х..знаеткто"
                    android:layout_marginEnd="16dp"
                    android:gravity="right"/>

                    </android.support.v7.widget.LinearLayoutCompat>
                     */
                }

                if (obj.bonus){
                    holder.itemView.setBackgroundColor(Color.parseColor("#f3fcff"))
                    holder.holderView.win_icon.visibility = View.VISIBLE
                } else {
                    holder.itemView.setBackgroundColor(Color.parseColor("#ffffff"))
                    holder.holderView.win_icon.visibility = View.GONE
                }
                holder.itemView.setOnClickListener {
                    val intent = Intent(holder.itemView.context, CheqView::class.java)
                    intent.putExtra(ScannerActivity.SCAN, obj.id)
                    intent.putExtra("CALL_FROM_LIST", true)
                    holder.itemView.context.startActivity(intent)
                }
            }
            1 -> {
                with(holder.holderView){
                    val obj = dataSet.get(position) as CheqHistoryHat
                    allCheqCount.text = obj.countCheq.toString()
                    loadingCheqCount.text =obj.contCheqInOrder.toString()
                    fullSum.text = obj.balls.toString()
                    itogoValue.text = "%.2f \u20BD".format(obj.fullSum)
            }
        }
    }
    }
}