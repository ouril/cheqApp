package ru.cheqin.cheqapp.history

import android.annotation.SuppressLint
import android.app.Fragment
import android.os.Build
import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.get
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.orhanobut.hawk.Hawk
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.history_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.cheqin.cheqapp.R


import ru.cheqin.cheqapp.cheq.CheqData
import ru.cheqin.cheqapp.cheq.CheqHistoryHat
import ru.cheqin.cheqapp.cheq.IsGetted
import ru.cheqin.cheqapp.dataBaseUtils.BillModel
import ru.cheqin.cheqapp.fragments.AbsResyclerFragment
import ru.cheqin.cheqapp.tools.*
import java.lang.Exception
import java.text.SimpleDateFormat

class CheqHistoryFragment: AbsResyclerFragment(), KodeinAware {

    override val kodein by closestKodein()
    val realm: Realm by instance()

    private val COUNT_CHEQ = "COUNT_CHEQ"
    private val FULL_PRICE = "FULL_PRICE"
    private val FULL_SUM = "FULL_SUM"
    lateinit var viewContext: View
    private var countOfCheqs: Int = 0
    private var countOfCheqsInOrder: Int = 0
    private var fullBalls: Int = 0

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        if (!(Hawk.contains(FULL_PRICE) && Hawk.contains(COUNT_CHEQ))){
            Hawk.put(FULL_PRICE, 0)
            Hawk.put(COUNT_CHEQ, 0)

            Hawk.put(FULL_SUM, 0)
        }

        val view = inflater!!.inflate(R.layout.history_fragment, container, false)
        viewContext = view

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?){
        val refresh = this.activity.findViewById<androidx.swiperefreshlayout.widget.SwipeRefreshLayout>(R.id.refresherHistory)
                as androidx.swiperefreshlayout.widget.SwipeRefreshLayout
        Hawk.init(this.activity).build()

        url = if (Hawk.get("IS_PROD")) SERVER else SERVERD
        val mocPhone = Hawk.get<String>(PHONE)
        getCachOfListSize()
        getCheqHistory(mocPhone)
        refresh.setOnRefreshListener {
            refresherHistory.isRefreshing = false
            getCheqHistory(mocPhone)
            //refresherHistory.isRefreshing = false
        }

    }

    @SuppressLint("CheckResult")
    private fun getCheqHistory(mocPhone: String?) {
        val data = realm.where(BillModel::class.java).findAll()
        val dataisNotGetted = realm.where(BillModel::class.java)
                .equalTo("isGetted", false)
                .equalTo("isDontGetted", false).findAll()
        countOfCheqsInOrder = dataisNotGetted.size
        countOfCheqs = realm.where(BillModel::class.java)
                .equalTo("isGetted", true).findAll().size
        postRxToServer(jsonListBuilder(CHEQ_HISTORY, mocPhone))!!.subscribe { pair, _ ->

            createOflineResycler(data, viewContext)
            val (response, result) = pair
            if (response.statusCode > 0) {

                if (response.statusCode in 1..399) {
                    val json = result.component1().toString()
                    val gson = Gson()
                    val res = gson.fromJson<JsonObject>(json)
                    if (!res.has("error")) {
                        val dataArray = res["result"][0]["list"]
                                .asJsonArray

                        var sum: Float = 0f
                        for (i in res["result"]
                                .asJsonArray[0]
                                .asJsonObject["list"]
                                .asJsonArray) {
                            sum += i["sum"].asString.toFloat()
                        }

                        fullBalls = res["result"]
                                .asJsonArray[0]
                                .asJsonObject["cheq_main_bonus"]
                                .asInt / 100

                        val idList = data.map { it.id }
                        val deleteList = idList.filter {
                            it !in dataArray.map { it["id"].asString }
                        }

                        deleteList.forEach {

                            val objForDel = realm.where(BillModel::class.java)
                                    .equalTo("id", it)
                                    .findFirst()

                            realm.beginTransaction()
                            objForDel!!.deleteFromRealm()
                            realm.commitTransaction()

                        }

                        dataArray.forEach {
                            realm.beginTransaction()
                            val itId = it["id"].asString
                            val action = if (itId !in idList) realm.createObject(BillModel::class.java, itId)
                            else data.find { it.id == itId }
                            with(action!!) {
                                if (this.sum != it["sum"].asString) this.sum = it["sum"].asString
                                if (this.date_time != it["date_time"]
                                                .asString) date_time = it["date_time"].asString
                                bonus = it["bonus"].asBoolean
                                val org = it["organization"].asJsonObject
                                organization = org["name"].asString
                                orgName = org["short_name"].asString
                                icon = org["tm_url"].asString
                                val isGet = try {
                                    it["status"].asString == IsGetted.success.toString()
                                } catch (e: Exception) {
                                    false
                                }
                                isGetted = isGet
                                isDontGetted = it["status"].asString == IsGetted.notFound.toString()
                            }
                            realm.commitTransaction()
                        }
                        createOflineResycler(data, viewContext)

                    }

                } else {
                    createOflineResycler(data, viewContext)
                }
            }
        }
    }

    fun getCachOfListSize() {
        val fullSum = Hawk.get(FULL_PRICE, 0)
        fullBalls = fullSum
        val countCheq = Hawk.get(COUNT_CHEQ, 0)
    }

    private fun createOflineResycler(data: RealmResults<BillModel>, view: View) {
        var fulSum: Double = 0.0
        val listData = mutableListOf<Any>()
        data.forEach {
                /**
                jsonData.add(
                        jsonObject(
                                id to it.id,
                                bonus to it.bonus,
                                sum to it.sum,
                                date_time to it.date_time,
                                isGetted to it.isGetted,
                                organization to it.organization,
                                short_name to it.orgName,
                                iconOrg to it.icon

                        )
                )
                */

            fulSum += it.sum.let {
                val answer = it!!.replace(',', '.')
                answer.toDouble()
            }

                listData.add(CheqData(
                        it.id,
                        it.bonus,
                        it.sum!!,
                        it.date_time,
                        SimpleDateFormat("dd-MM-yy").parse(it.date_time).time,
                        it.isGetted,
                        it.organization,
                        it.orgName,
                        it.icon,
                        it.isDontGetted
                ))

            }
        listData.sortedBy {
            it as CheqData
            it.unixTime
        }
        listData.reverse()
        listData.add(0, CheqHistoryHat(countOfCheqs, countOfCheqsInOrder, Hawk.get(FULL_PRICE), fulSum))

        createNewResycler(view, listData, R.id.recyclerView)
    }

    override fun createRecycler(view: View, data: JsonArray, resyclerId: Int, isOfline: Boolean) {
        /**
        layoutManagerRecycler = LinearLayoutManager(view.context)
        adapterHistory = HistoryAdapter(data)
        view.findViewById<RecyclerView>(resyclerId).apply {
            setHasFixedSize(true)
            layoutManager = layoutManagerRecycler
            adapter = adapterHistory
        }
        */
    }

    fun createNewResycler(view: View, data: List<Any>, resyclerId: Int ){
        view.findViewById<androidx.recyclerview.widget.RecyclerView>(resyclerId).apply {
            setHasFixedSize(true)
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(view.context)
            adapter = HistoryAdapter(data)
        }
    }


    override fun postRxToServer(data: JsonObject): Single<Pair<Response, Result<String, FuelError>>>? {
        Log.d("JSON__DATA", data.toString())
        return url.httpPost().body(data.toString()).rx_responseString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
    }

    companion object {
        val id = "id"
        val bonus = "bonus"
        val sum = "sum"

        fun newInstance(): Fragment {
            return CheqHistoryFragment()
        }
    }

}