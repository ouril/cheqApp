package ru.cheqin.cheqapp

import android.app.Application
import com.orhanobut.hawk.Hawk
import io.realm.Realm
import io.realm.RealmConfiguration

import org.kodein.di.Kodein
import org.kodein.di.Kodein.Companion.lazy
import org.kodein.di.KodeinAware
import org.kodein.di.generic.*
import ru.cheqin.cheqapp.cheq.CheqModel
import ru.cheqin.cheqapp.profile.ProfileModel
import ru.cheqin.cheqapp.tools.CheqLocationListener

class CheqApplication : Application(), KodeinAware {

    override fun onCreate() {
        Hawk.init(this).build()
        Realm.init(this)
        super.onCreate()
    }

    override public val kodein = lazy {
        bind<Realm>() with singleton {

            val config = RealmConfiguration.Builder()
                    .name("fullModelv1.realm").build()
             Realm.getInstance(config)
        }
        bind<CheqLocationListener>() with singleton {
            CheqLocationListener.setUpLocationListener(this@CheqApplication)
            CheqLocationListener()
        }
        bind<ProfileModel>() with singleton {
            ProfileModel()
        }
        bind<CheqModel>() with singleton {
            CheqModel()
        }


    }
}