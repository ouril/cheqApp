package ru.cheqin.cheqapp.cheq

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import ru.cheqin.cheqapp.profile.Answer
import ru.cheqin.cheqapp.profile.Response


interface CheqScanedPost {

    @POST("tnt")
    fun getCheq(@Body cheq: CheqRequest): Single<Cheq>
}