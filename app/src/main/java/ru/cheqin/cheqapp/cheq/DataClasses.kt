package ru.cheqin.cheqapp.cheq

import com.google.gson.JsonElement
import java.util.*


data class CheqRequest(
        val id: Int,
        val params: List<Any>,
        val method: String
)

data class GeoData(
        val longitude: String,
        val geolocation: Boolean,
        val latitude: String
)


data class Cheq (
        val id: Int,
        val result: CheqDataContainer,
        val bin_bon_val: List<Any>?
)

data class CheqDataContainer (
        val im_owner: Boolean,
        val organization: CheqOrgInfo,
        val cheq_main_bonus: Int?,
        val scan_time: Int?,
        val status: String,
        val document: CheqDocument
)

data class CheqDocument (
        val reciept: CheqDocumentReciept
)

data class CheqDocumentReciept(
      val cashTotalSum: Int,
      val kktRegId: String,
      val fiscalDocumentNumber: Int,
      val operationType: Any?,
      val fiscalDriveNumber: String,
      val fiscalSign: Int,
      val shiftNumber: Int?,
      val userInn: String?,
      val dateTime: String,
      val receiptCode: Int?,
      val requestNumber: Int?,
      val nds18: Int?,
      val retailPlaceAddress: String?,
      val address: String?,
      val totalSum: Int,
      val taxationType: Any?,
      val operator: String?,
      val user: String?,
      val ecashTotalSum: Any?,
      val items: List<Item>
)


data class Item(
        val sum: Int,
        val cheq_id: Int,
        val quantity: Int,
        val name: String,
        val price: Int
)

data class CheqOrgInfo(
        val tm_url: String,
        val name: String,
        val short_name: String
)

data class Currency(val pic: String, val count: Int)

data class Summa(val sum: String, val name: String)

data class QRString(val text:String)

data class StringData(val name: String)

data class CheqItemAndNumber(val i:Int, val json: JsonElement)

data class CheqData(
        var id: String = "",
        var bonus: Boolean=false,
        var sum: String = "",
        var date_time: String?=null,
        var unixTime: Long?=null,
        var isGetted: Boolean=false,
        var organization: String?=null,
        var short_name: String?=null,
        var iconOrg: String?=null,
        var notFound: Boolean = false
)

data class CheqHistoryHat(
        val countCheq: Int=0,
        val contCheqInOrder: Int=0,
        val balls: Int=0,
        val fullSum: Double=0.0
)

data class CheqItemHat(
        var icon:String? = null,
        var name:String? = null,
        var inn:String? = null,
        var addr:String? = null,
        var date: Date? = null,
        var time: String? =null

)

data class CheqInfo(
        var nalog: String? = null,
        var kkt: String? = null,
        var fn: String? = null,
        var fd: String? = null,
        var fpd: String? = null,
        var billNumber:String? = null,
        var cashMen:String? = null,
        var groupNumber:String? = null
)


data class CheqGift(
        var giftId: String? = null,
        var actionId: String? = null,
        var name: String? = null,
        var pic: String? = null,
        var desc: String = "",
        var code: String? = null,
        var isSecret: Boolean = false
)


enum class IsGetted{
    success, inProcess, notFound
}