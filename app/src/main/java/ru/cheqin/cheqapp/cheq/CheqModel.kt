package ru.cheqin.cheqapp.cheq

import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response

import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import ru.cheqin.cheqapp.AbsWebModel
import java.lang.Exception


class CheqModel: AbsWebModel(), CheqContract.Model{


    override fun getCheq(qr: String, phone: String, url: String, longitude: String, latitude: String):
            Single<Cheq>? {

        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val service = retrofit.create(CheqScanedPost::class.java)
        val geo = GeoData(longitude, true, latitude)
        val time = System.currentTimeMillis() / 1000L
        val request = CheqRequest(
                1,
                listOf(phone, qr, geo, time.toInt()),
                "bill_api.get_scanned_bill")

        return service.getCheq(request)
    }


    override fun getCheqFuel(qr: String, phone: String, url: String, longitude: String, latitude: String):
            Single<Pair<Response, Result<String, FuelError>>>? {
        try {
            val jsonCheq = jsonObject(
                    "method" to "bill_api.get_scanned_bill",
                    "params" to jsonArray(
                            phone,
                            qr,
                            jsonObject(
                                    "longitude" to longitude,
                                    "geolocation" to true,
                                    "latitude" to latitude
                            ),
                            System.currentTimeMillis() / 1000L
                    ),
                    "id" to 1
            )

            Log.d("CHEQ_REQUEST_LOG", "json ---> $jsonCheq")

            return url.httpPost().body(jsonCheq.toString())
                    .rx_responseString()

        } catch (e: Exception) {
            return null
        }
    }


}











