package ru.cheqin.cheqapp.cheq

import android.location.Location
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.result.Result
import com.google.gson.JsonObject
import io.reactivex.Single
import ru.cheqin.cheqapp.tools.CheqLocationListener

interface CheqContract {

    interface View {

        fun showCheq(data: ArrayList<Any?>, isOwner: Boolean)

    }
    interface Model {
        fun getCheq(qr: String, phone: String, url: String, longitude: String, latitude: String):
                Single<Cheq>?

        fun getCheqFuel(qr: String, phone: String, url: String, longitude: String, latitude: String):
                Single<Pair<Response, Result<String, FuelError>>>?
    }


    interface Presenter {
        fun attach(context: View)

        fun detach()

        fun buildCheq(qr: String, phoneNumber: String, location: Location?, url: String)
    }
}