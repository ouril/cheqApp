package ru.cheqin.cheqapp.cheq

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.orhanobut.hawk.Hawk
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.cheq_activity.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.*

class CheqView : AppCompatActivity(), KodeinAware, CheqContract.View {
    override val kodein by closestKodein()

    lateinit var url: String
    lateinit var scan: String
    lateinit var phone: String
    var presenter: CheqPresenter? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RxJavaPlugins.setErrorHandler { throwable ->
            //            Log.d("RX_ERROR", throwable.localizedMessage)
            throwable.printStackTrace()
        }
        setContentView(R.layout.cheq_activity)

        Hawk.init(this).build()
        CheqLocationListener.setUpLocationListener(this)
        url = if (Hawk.get("IS_PROD")) SERVER else SERVERD
        scan = intent.extras.getString("SCAN")
        presenter = CheqPresenter(kodein)
        phone = Hawk.get(PHONE)

        try {
            presenter!!.attach(this)
            presenter!!.buildCheq(
                    scan,
                    phone,
                    CheqLocationListener.imHere,
                    url
            )
        } catch (e:kotlin.KotlinNullPointerException) {
            e.printStackTrace()
        }
        back.setOnClickListener {
            finish()
        }
    }

    override fun onDestroy() {
        presenter?.detach()
        super.onDestroy()
    }

    override fun showCheq(data: ArrayList<Any?>, isOwner: Boolean) {

        with(listRes) {
            setHasFixedSize(true)
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@CheqView)
            adapter = CheqAdapter(data, isOwner)
        }
        listRes.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        texProgressBar.visibility = View.GONE
        if (!isOwner) longToast("Чек был получен другим пользователем и не будет сохранен в списке чеков")
    }
}