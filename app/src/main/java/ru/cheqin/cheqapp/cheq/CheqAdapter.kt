package ru.cheqin.cheqapp.cheq

import android.annotation.SuppressLint
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.cheq_item.view.*
import ru.cheqin.cheqapp.R
import kotlinx.android.synthetic.main.cheq_hat_item.view.*
import kotlinx.android.synthetic.main.cheq_info_item.view.*
import kotlinx.android.synthetic.main.item_cheq_list.view.*
import kotlinx.android.synthetic.main.money_item.view.*
import kotlinx.android.synthetic.main.qr_item.view.*
import kotlinx.android.synthetic.main.string_item.view.*
import kotlinx.android.synthetic.main.sum_item.view.*
import org.jetbrains.anko.startActivity
import ru.cheqin.cheqapp.MoneyActivity
import ru.cheqin.cheqapp.actions.OneActionActivity
import ru.cheqin.cheqapp.adapters.*
import ru.cheqin.cheqapp.dataBaseUtils.CurrencyModel
import ru.cheqin.cheqapp.tools.ID
import ru.cheqin.cheqapp.tools.buildQrBitmap
import ru.cheqin.cheqapp.tools.loadImage
import ru.cheqin.cheqapp.wallet.GiftActivity
import java.text.SimpleDateFormat


class CheqAdapter(
        val myDataset: ArrayList<Any?>,
        val isOwner: Boolean

):
        androidx.recyclerview.widget.RecyclerView.Adapter<CheqAdapter.ViewHolder>() {



    override fun getItemViewType(position: Int): Int {

        return when(myDataset[position]) {
            is CheqItemAndNumber -> {
                super.getItemViewType(position)
            }
            is Currency -> {
                1
            }
            is StringData -> {
                2
            }
            is Summa -> {
                3
            }
            is QRString -> {
                4
            }
            is String -> {
                5
            }
            is CheqData ->{
                6
            }
            is CheqInfo -> {
                7
            }
            is CheqItemHat -> {
                8
            }
            is CheqGift -> {
                9
            }
            else -> 10
        }

    }

    inner class ViewHolder(textView: View, viewType: Int = 0) : androidx.recyclerview.widget.RecyclerView.ViewHolder(textView),
            View.OnClickListener {
        val holderType: Int = viewType
        val holderView = textView

        override fun onClick(v: View?) {
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        when(viewType) {
            0 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.cheq_item, parent, false)

                return ViewHolder(view)
            }
            1 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.money_item, parent, false)

                return ViewHolder(view, viewType)

            }
            2 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.string_item, parent, false)

                return ViewHolder(view, viewType)
            }
            3 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.sum_item, parent, false)

                return ViewHolder(view, viewType)
            }
            4 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.qr_item, parent, false)

                return ViewHolder(view, viewType)
            }
            5 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.dot_dot_item, parent, false)

                return ViewHolder(view, viewType)
            }
            6 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_cheq_list, parent, false)

                return ViewHolder(view, viewType)
            }
            7 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.cheq_info_item, parent, false)

                return ViewHolder(view, viewType)
            }
            8 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.cheq_hat_item, parent, false)

                return ViewHolder(view, viewType)
            }
            9 -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.gift_item_cheq, parent, false)

                return ViewHolder(view, viewType)
            }

            else -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.empty_item, parent, false)

                return ViewHolder(view, viewType)
            }

        }


    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(holder.holderType){
            0 -> {
                val prodFirst = myDataset[position] as CheqItemAndNumber
                val prod = prodFirst.json.asJsonObject
                val item = prodFirst.i
                with(holder.holderView) {
                    title.text = prod.get("name").asString.trim().trim('"')
                    num.text = "${item}"
                    desc.text = "${prod.get("price").asDouble / 100}"
                    colvo.text = "${prod.get("quantity").asInt}"
                    sumItem.text = "${prod.get("sum").asDouble / 100}"

                    if (prod.has("cheq_bonus") && isOwner) {

                        bonus_field.visibility = View.VISIBLE
                        line.visibility = View.VISIBLE
                        loadImage(prod.get("cheq_bonus_valuta_type").asString, bonus_logo)
                        main_cont.background = ContextCompat
                                .getDrawable(holder.itemView.context, R.drawable.bonus_fon)
                        bonus_count.text = "+ ${prod.get("cheq_bonus").asInt / 100}"
                        holder.itemView.setOnClickListener {
                            holder.itemView.context.startActivity<MoneyActivity>(
                                    ID to prod.get("cheq_promo_id").asString,
                                    "sum" to prod.get("cheq_bonus").asString,
                                    "pic" to prod.get("cheq_bonus_valuta_type").asString,
                                    "name" to prod.get("name").asString

                            )
                        }

                    } else {
                        bonus_field.visibility = View.GONE
                        line.visibility = View.GONE
                        main_cont.background = ContextCompat
                                .getDrawable(holder.itemView.context, R.color.white)
                    }
                }



            }
            1 -> {
                val text = myDataset[position] as Currency
                holder.holderView.walleyScore.text = text.count.toString()

                loadImage(text.pic, holder.holderView.imageWalley)
            }
            2 -> {
                val text = myDataset[position] as StringData
                holder.holderView.textView8.text = text.name
            }
            3 -> {
                val text = myDataset[position] as Summa
                holder.holderView.textViewName.text = text.name
                holder.holderView.textViewNumber.text =text.sum

            }
            4 -> {
                val text = myDataset[position] as QRString
                holder.holderView.imageViewQR.setImageBitmap(buildQrBitmap(text.text))

            }
            6 -> {
                val obj = myDataset.get(position) as CheqData
                with(holder.holderView) {

                    if (obj.isGetted) {
                        mainText.text = "Чек получен"
                        imageState.setImageResource(R.drawable.ic_success_cheq)
                    } else if(obj.notFound){
                        mainText.text = "Чек не найден"
                        imageState.setImageResource(R.drawable.ic_not_found)
                    } else {
                        mainText.text = "Чек в обработке"
                        imageState.setImageResource(R.drawable.ic_group_6)
                    }
                    if (obj.bonus) win_icon.visibility = View.VISIBLE
                    txtOptionDigit.text = "${obj.sum} \u20BD"
                    val dt = obj.date_time!!.replace('T', ' ')
                    val (date
                            , time)
                            = dt
                            .replace("\"", "")
                            .split(" ")
                    val dateForString = SimpleDateFormat("yyyy-MM-dd").parse(date)
                    val resultDateFrString = SimpleDateFormat("dd MMMM yyyy").format(dateForString)
                    textTime.text = "Отсканировано $resultDateFrString года"

                }
            }
            7 -> {
                val obj = myDataset.get(position) as CheqInfo
                with(holder.holderView){
                    fdLine.visibility = View.GONE
                    obj.fd?.let{
                        fdLine.visibility = View.VISIBLE
                        fdText.setText(it)
                    }
                    fnLine.visibility = View.GONE
                    obj.fn?.let {
                        fnLine.visibility = View.VISIBLE
                        fnText.setText(it)
                    }
                    fdpLine.visibility = View.GONE
                    obj.fpd?.let{
                        fdpLine.visibility = View.VISIBLE
                        fdpText.setText(it)
                    }
                    KKTLine.visibility = View.GONE
                    obj.kkt?.let{
                        KKTLine.visibility = View.VISIBLE
                        KKTText.setText(it)
                    }
                    cashMenLine.visibility = View.GONE
                    obj.cashMen?.let{
                        cashMenLine.visibility = View.VISIBLE
                        cashMenText.setText(it)
                    }
                    nalogLine.visibility = View.GONE
                    obj.nalog?.let{
                        if(it != "null") {
                            nalogLine.visibility = View.VISIBLE
                            nalogText.setText(it)
                        }
                    }
                    workGroupLine.visibility = View.GONE
                    obj.groupNumber?.let{
                        workGroupLine.visibility = View.VISIBLE
                        workGroupText.setText(it)
                    }
                    billNumberLine.visibility = View.GONE
                    obj.billNumber?.let{
                        billNumberLine.visibility = View.VISIBLE
                        billNumberText.setText(it)
                    }

                }
            }
            8 -> {
                val obj = myDataset.get(position) as CheqItemHat
                with(holder.holderView){
                    var iconString: String
                    if (obj.icon!!.length > 1){
                        billIcon.visibility = View.GONE
                        loadImage(obj.icon!!, billIconPic)
                    } else {
                        billIconPic.visibility = View.GONE
                        obj.icon?.let{
                            billIcon.setText(it)
                        }
                    }


                    addr.visibility = View.GONE
                    obj.addr?.let{
                        addr.setText(it)
                        addr.visibility = View.VISIBLE
                    }
                    companyName.visibility =View.GONE
                    obj.name?.let{
                        companyName.visibility = View.VISIBLE
                        companyName.setText(it.trim())
                    }
                    inn.visibility = View.GONE
                    obj.inn?.let{
                        inn.visibility = View.VISIBLE
                        inn.text = "ИНН: $it"
                    }

                    billDate.text = "Чек выдан: ${SimpleDateFormat("dd MMMM yyyy")
                            .format(obj.date)} года в ${obj.time}"
                }
            }
            9 -> {
                val obj = myDataset[position] as CheqGift
                Log.d("GIFT", obj.toString())
                holder.itemView.setOnClickListener {
                    holder.itemView.context.startActivity<GiftActivity>(
                            "pic" to obj.pic,
                            "id" to obj.actionId,
                            "name" to obj.name,
                            "desc" to obj.desc,
                            "code" to obj.giftId,
                            "gift_code" to obj.code,
                            "isSecret" to obj.isSecret

                    )
                }

            }

        }


    }

    override fun getItemCount() = myDataset.size

}


