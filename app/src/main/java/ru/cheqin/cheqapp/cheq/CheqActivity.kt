package ru.cheqin.cheqapp.cheq


import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.*
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.orhanobut.hawk.Hawk

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import kotlinx.android.synthetic.main.cheq_activity.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import ru.cheqin.cheqapp.congrads.CongradsActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.scaner.ScannerActivity
import ru.cheqin.cheqapp.dataBaseUtils.BillModel
import ru.cheqin.cheqapp.tools.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class CheqActivity: AppCompatActivity(), KodeinAware {
    override val kodein by closestKodein()
    val locationListener: CheqLocationListener by instance()
    val realm: Realm by instance()
    lateinit var url: String


    lateinit var scan: String
    lateinit var scanDict: HashMap<String,String>
    lateinit var dataCheq: BillModel
    var calledFromList: Boolean = false

    var block: Int = 0

    companion object {
        const val CHEQ_JSON_LOG = "CHEQ_JSON_LOG"
        const val BILL_GIFT = "bill_gift"
        const val ERRORS_KEY = "error"
        const val RESULT_JSON = "result"
        const val IM_OWNER = "im_owner"
        const val URL_TRY = "urlTry"
        const val URL_CHEQ ="url"
        const val SUM = "sum"
        const val DATE = "date"
        const val BILL_BON_VAL = "bill_bon_val"
        const val MAIN_BONUS = "cheq_main_bonus"
        const val LOCATION = "LOCATION"
    }

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cheq_activity)
        Hawk.init(this).build()
        url = if (Hawk.get("IS_PROD")) SERVER else SERVERD
        val name = Hawk.get<String>(NAME)
        val phone = Hawk.get<String>(PHONE)
        val pass = Hawk.get<String>(PASSWORD)
        scan = intent.extras.getString(ScannerActivity.SCAN)!!
        scanDict = stringBuild(scan)

        //dataCheq = realm.where(BillModel::class.java).equalTo("id", scan).findFirst()

        calledFromList = intent.extras.getBoolean("CALL_FROM_LIST", false)

        val scanMap = scanMaping(scan.toString())
        val jsonCheq = jsonObject(
                "method" to "bill_api.get_bill",
                "params" to jsonArray(
                        phone,
                        scan
                ),
                "id" to 1
        )

        postRxToServer(jsonCheq.toString())!!.subscribe { pair, _ ->
            val (response, result) = pair
            if (response.statusCode in 1..399) {

                val res = getJsonObjOfResult(result)


                if (res.has(ERRORS_KEY)){
                    Log.d(CHEQ_JSON_LOG, response.statusCode.toString())
                    //snackbar(allCheq, "Чек проверен")
                    val now = Calendar.getInstance().getTime()
                    Log.d(CHEQ_JSON_LOG, now.toString())
                    val (cheqDate, cheqTime) = scanMap[DATE]!!.split('T')

                    val cheqDateObj = "${cheqDate.substring(0,4)}-${cheqDate.substring(4,6)}-${cheqDate.substring(6,8)}"
                    val cheqDataSum = scanDict["summa"]!!.toInt().toDouble() / 100


                    realm.beginTransaction()
                    val cheq = BillModel()
                    cheq.id = scan
                    cheq.isGetted = false
                    cheq.sum = "%.2f".format(cheqDataSum).replace(',', '.')
                    cheq.scan_time = now
                    cheq.date_time = "$cheqDateObj $cheqTime"
                    realm.insertOrUpdate(cheq)
                    realm.commitTransaction()
                    sendToServer(phone)

                    /**
                     * if in result we have "error" key that's mean that on server we don't have this ru.cheqin.cheqapp.cheq
                     * Json with mistake:
                     * {"id":1,"error":{"message":"Bill not found","code":-32768}}


                    tringRequest(scanMap)!!.subscribe { pairTrying, errors ->
                        val (responseTryin, resultTrying) = pairTrying
                        Log.d(CHEQ_JSON_LOG, " ERRORS  ->  $errors")

                        if (responseTryin.statusCode in 1..399) {
                            when(resultTrying) {
                                is Result.Success -> {

                                    //getCheq(phone, pass, scanMap[CheqActivity.URL_CHEQ].toString())
                                }
                                is Result.Failure -> {
                                    Log.d(CHEQ_JSON_LOG, response.statusCode.toString())
                                    toast("Что-то пошло не так, попробуйте еще раз")
                                    finish()
                                }
                                else -> {
                                    Log.d(CHEQ_JSON_LOG, response.statusCode.toString())
                                    toast("Что-то пошло не так, попробуйте еще раз")
                                    finish()
                                }
                            }

                        } else {
                            finish()
                            toast("Чек не прошел проверку")
                            Log.d(CHEQ_JSON_LOG, responseTryin.statusCode.toString())
                        }
                    }
                     */

                } else {
                    buildCheq(res)
                }


            } else {

                finish()
                toast("Проверьте соединение с интернетом")
            }
        }
    }

    private fun buildCheq(res: JsonObject) {
        //cardWithBal.visibility = View.VISIBLE
        //nameLine.visibility = View.VISIBLE

        println("--------------> $res")

        drawCheq()
        val jsonResultJson = res[RESULT_JSON]
                .asJsonArray[0]
                .asJsonObject

        val jsonReceipt = jsonResultJson["document"]
                .asJsonObject["receipt"]
                .asJsonObject

        val (date, time) = jsonReceipt["dateTime"]
                .asString
                .split("T")


        val is_owner = jsonResultJson[IM_OWNER].asBoolean
        val bonVal = jsonResultJson[BILL_BON_VAL].asJsonArray
        var isGetted = jsonResultJson["status"].asString
        val bonusMain = try {
            jsonResultJson[MAIN_BONUS].asInt / 100
        } catch (e:Exception){
            0
        }

        val data = ArrayList<Any?>()
        val (cheqItems, sumString) = getStringSumAndCheqItems(res)

        if (is_owner) {
            dataCheq = realm.where(BillModel::class.java).equalTo("id", scan).findFirst()!!

            if (!calledFromList) {
                with(CongradsActivity) {
                    startActivity<CongradsActivity>(
                            IS_GETTED to dataCheq.isGetted,
                            SCAN to scan,
                            BONUS to bonusMain,
                            DATE to dataCheq.date_time
                    )
                }
                finish()
            }


            data.add(CheqData(
                    dataCheq.id,
                    dataCheq.bonus,
                    dataCheq.sum!!,
                    dataCheq.date_time,
                    SimpleDateFormat("dd-MM-yy").parse(dataCheq.date_time).time,
                    dataCheq.isGetted,
                    dataCheq.organization,
                    dataCheq.shortOrgName,
                    dataCheq.icon

            ))

            data.add(null)

            val cheqHat = CheqItemHat()

            with(cheqHat) {
                dataCheq.organization?.let { name = it }
                inn = stringPuterFromJson(jsonReceipt, "userInn")
                dataCheq.shortOrgName?.let { icon = it.substring(0, 1).toUpperCase() }
                this.date = SimpleDateFormat("yyyy-MM-dd").parse(dataCheq.date_time)
                this.time = time



                //TODO: ИСПРАВИТЬ!!!!!!!!


                icon = "W"
            }

            data.add(cheqHat)
        } else {
            data.add{
                if (!calledFromList) {
                    with(CongradsActivity) {
                        startActivity<CongradsActivity>(
                                IS_GETTED to true,
                                SCAN to scan,
                                BONUS to bonusMain,
                                DATE to rightDate(jsonReceipt["dateTime"].asString)
                        )
                    }
                    finish()
                }

                val summaInFloat = jsonReceipt["totalSum"].asString.toDouble()/100

                data.add(CheqData(
                        scan,
                        false,
                        "%.2f".format(summaInFloat),
                        rightDate(jsonReceipt["dateTime"].asString),
                        SimpleDateFormat("dd-MM-yy").parse(rightDate(jsonReceipt["dateTime"].asString)).time,
                        true,
                        stringPuterFromJson(jsonReceipt, "user")?.trim(),
                        "Ц",
                        null

                ))

                data.add(null)

                val cheqHat = CheqItemHat()

                with(cheqHat) {
                    inn = stringPuterFromJson(jsonReceipt, "userInn")
                    this.date = SimpleDateFormat("yyyy-MM-dd").parse(dataCheq.date_time)
                    this.time = time


                    //TODO: ИСПРАВИТЬ!!!!!!!!

                    icon = "g"//dataCheq.orgName?.substring(0, 1)
                }

                data.add(cheqHat)
            }
        }
        /**
        if (bonVal.size() > 0){
            bonVal.forEach {
                data.add(Currency(it.asJsonObject["link"].asString, it.asJsonObject["value"].asInt))
            }
            data.add(null)
        }

        data.add(StringData("$date в $time"))

        if (jsonReceipt.has("userInn")) {
            val inn = jsonReceipt["userInn"].asString
            data.add(StringData("ИНН: $inn"))
        }

        if (jsonReceipt.has("user")) {
            val name = jsonReceipt["user"].asString
            data.add(StringData(name))
        }
*/
        data.add("dot")
        var itemNumber = 0
        for (i in cheqItems) {
            itemNumber++
            data.add(CheqItemAndNumber(itemNumber, i))
        }
        data.add("dot")
        data.add(Summa(sumString, "Итого"))

        /**
        data.add(StringData("ФН: ${jsonReceipt["fiscalDriveNumber"].asString}"))
        data.add(StringData("ФД: ${jsonReceipt["fiscalDocumentNumber"].asInt}"))
        data.add(StringData("ФПД: ${jsonReceipt["fiscalSign"].asInt.toString().replace("-", "")}"))
        */
        data.add(null)
        val cheqInfo = CheqInfo()
        with(cheqInfo){
            kkt = stringPuterFromJson(jsonReceipt, "kktRegId")?.trim()
            cashMen = stringPuterFromJson(jsonReceipt, "operator")
            nalog = intPuterFromJson(jsonReceipt, "taxationType").toString()
            fd = intPuterFromJson(jsonReceipt, "fiscalDocumentNumber").toString()
            fn = stringPuterFromJson(jsonReceipt, "fiscalDriveNumber")
            fpd = intPuterFromJson(jsonReceipt, "fiscalSign").toString().replace("-", "")
        }
        data.add(cheqInfo)


        data.add(null)
        data.add(QRString(scan))


        //val bonus = jsonResultJson[MAIN_BONUS].asInt
        //val bonusRes = "+ " + (bonus.toLong() / 100).toString()
        //val sumForView = sumString.split(".") + " \u20BD"
        //textView6.setText(sumString)
        //cheqScore.setText(bonusRes)

        /**
         * is_owner will be used for snackbar
         */

        // println("---------------------------->           ${jsonResultJson}")
        realm.beginTransaction()
        val cheqObject = BillModel()
        with(cheqObject) {
            val summaInFloat = jsonReceipt["totalSum"].asString.toDouble()/100
            sum = "%.2f".format(summaInFloat)
            date_time = jsonReceipt["dateTime"].asString
            organization = try {
                jsonReceipt["user"].asString
            } catch (e: java.lang.IllegalStateException){
                null
            }
                    //isGetted = true
        }
        realm.insertOrUpdate(cheqObject)
        realm.commitTransaction()


        /**
        if (is_owner) {
            snackbar(allCheq, "Чек в вашей базе")
        } else {
            snackbar(allCheq, "Враги забрали чек")
        }
        */
        createCheqRecycle(
                data,
                true
        )
    }

    private fun intPuterFromJson( jsonReceipt: JsonObject, key:String): Int? {
        return if (jsonReceipt.has(key))
            try {
                jsonReceipt[key].asInt
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        else null
    }

    private fun stringPuterFromJson(jsonReceipt: JsonObject, key:String): String? {
        return if (jsonReceipt.has(key))
            try {
                jsonReceipt[key].asString
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        else null
    }


    fun drawCheq() {

        progressBar.visibility = View.GONE
        texProgressBar.visibility = View.GONE
        listRes.visibility = View.VISIBLE

    }


    fun getJsonObjOfResult(result: Result<String, FuelError>): JsonObject {

        Log.d(CHEQ_JSON_LOG, result.component1().toString())

        val json = result.component1().toString()
        val gson = Gson()
        val res = gson.fromJson<JsonObject>(json)
        return res
    }

    private fun getStringSumAndCheqItems(res: JsonObject): Pair<JsonArray, String> {
        var cheq: JsonObject
        if (res.has(CheqActivity.RESULT_JSON)) {
            cheq = try {
                res[CheqActivity.RESULT_JSON]
                        .asJsonArray[0]
                        .asJsonObject["document"]
                        .asJsonObject["receipt"]
                        .asJsonObject
            } catch (e: java.lang.IllegalStateException) {
                res["document"]
                        .asJsonObject["receipt"]
                        .asJsonObject
            }
        } else {
            cheq = res["document"]
                    .asJsonObject["receipt"]
                    .asJsonObject
        }
        val cheqItems = cheq["items"].asJsonArray
        val sum = cheq["totalSum"].asDouble
        val sumString = "${"%.2f".format(sum / 100).replace(',', '.')}\u20BD"

        return Pair(cheqItems, sumString)
    }


    private fun tringRequest(url: Map<String, String>): Single<Pair<Response, Result<String, FuelError>>>? {
        // println("------------------------> ${url[URL_TRY]}")

        return url[URL_TRY]!!.httpGet()
                .header(headers())
                .rx_responseString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())

    }


    /**
     *
     * Request methods
     *
     */


    private fun getCheq(phone: String, password: String, urlAdress: String, bl: Int = 0) {

        block = bl
        /**
         * This is request for nalog.ru
         */

        urlAdress.httpGet()
                .authenticate(phone, password)

                .header(headers()).responseString {

                    request, response, result ->
                    Log.d(CHEQ_JSON_LOG, "count: $block")
                    Log.d(CHEQ_JSON_LOG, response.contentLength.toString())
                    Log.d(CHEQ_JSON_LOG, response.statusCode.toString())

                    when (result) {
                        is Result.Success -> {

                            if (response.statusCode == 202 && block < 15) {
                                block++
                                getCheq(phone, password, urlAdress, block)

                            } else if (response.statusCode == 202 && block >= 15){

                                toast("Возможно что чек еще не поступил в налоговую")
                                finish()

                            } else if (response.statusCode == 403) {
                                Hawk.init(this).build()
                                Hawk.delete(PHONE)
                                Hawk.delete(NAME)
                                finish()
                                toast("ОШИБКА С ПАРОЛЕМ")
                                finish()

                            } else if (response.statusCode == 200) {

                                val res = getJsonObjOfResult(result)
                                val (cheqItems, sumString) = getStringSumAndCheqItems(res)
                                drawCheq()
                                //cardWithBal.visibility = View.VISIBLE
                                //nameLine.visibility = View.VISIBLE
                                val time = res["document"]
                                        .asJsonObject["receipt"]
                                        .asJsonObject["dateTime"]
                                        .asString
                                        .split("T")[0]

                                //textView3.setText(time)
                                //textView6.setText(sumString)

                                val totalSum = res["document"]
                                        .asJsonObject["receipt"]
                                        .asJsonObject["totalSum"]
                                        .asInt / 2000

                                val totalSumString = "+ " + totalSum.toString()
                                //cheqScore.setText(totalSumString)

                                //sendToServer(res, phone)

                            } else if (response.statusCode == 406){

                                toast("Код ответа сервера ${response.statusCode} это значит, что ${response.responseMessage}")
                                finish()

                            } else {
                                dontFindCheck()
                            }
                        }
                        is Result.Failure -> {

                            block = block++
                            if (response.statusCode == 202 && block < 5) {

                                this.getCheq(phone, password, urlAdress, block)

                            } else if (response.statusCode == 202 && block < 5 && block > 3) {

                                block = block++
                                this.getCheq(phone, password, urlAdress, block)

                            } else if (response.statusCode == 403) {


                                toast("Код ответа сервера ${response.statusCode} это значит, что ${response.responseMessage}")

                                Hawk.init(this).build()
                                Hawk.delete(PHONE)
                                Hawk.delete(NAME)
                                finish()

                            } else  if (response.statusCode == 406){

                                toast("Код ответа ${response.statusCode} возможно что это старый чек и его уже нет.")
                                finish()
                            } else {
                                dontFindCheck()
                            }
                        }
                    }
                }
    }

    fun dontFindCheck() {
        infoIcon.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        texProgressBar.visibility = View.GONE
        lets_return.visibility = View.VISIBLE
        answer_no.visibility = View.VISIBLE
        answer_no_grey.visibility = View.VISIBLE
        lets_return.setOnClickListener {
            finish()
        }
    }


    private fun sendToServer(phone: String) {
        CheqLocationListener.setUpLocationListener(this)
        val locate = CheqLocationListener.imHere

        var jsonRes: JsonObject
        if (locate != null) {
            Log.d(LOCATION, "${locate.longitude}  ~  ${locate.latitude} ")
            jsonRes = jsonBuilderWithGeo(phone, scan,
                    //json,
                    true, locate.latitude.toString(), locate.longitude.toString())
        } else {
            jsonRes = jsonBuilderWithGeo(phone, scan//,
            //        json
            )
        }


        Log.d("TO_OUR_SERVER", jsonRes.toString())
        url.httpPost()
                .body(jsonRes.toString())
                .responseString { request, response, result ->
                    when (result) {
                        is Result.Failure -> {
                            //snackbar(view = allCheq, message = "Возникла проблема в работе " +
                                 //   "с сервером. Код ошибки ${response.statusCode}")

                        }
                        is Result.Success -> {

                            val res = getJsonObjOfResult(result)
                            buildCheq(res)
                            //longSnackbar(allCheq, "Чек получен и обработан. Вы можете узнать есть ли подарочные былы в Истории чеков")

                        }
                    }
                }
    }


    private fun postRxToServer(data: String): Single<Pair<Response, Result<String, FuelError>>>? {
        println(data)


        return url.httpPost().body(data).rx_responseString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
    }


    /**
     * Stringbuilding methods
     */


    private fun rightDate(str: String): String{
        val res:String


        if (str.length > 14) {
            res = "${str.substring(0, 4)}-${str.substring(4, 6)}-${str.substring(6, 8)}${str.substring(8, 11)}:" +
                    "${str.substring(11, 13)}:${str.substring(13, 15)}"
        } else {
            res = "${str.substring(0, 4)}-${str.substring(4, 6)}-${str.substring(6, 8)}${str.substring(8, 11)}:" +
                    "${str.substring(11, 13)}"
        }
        return res
    }

    private fun scanMaping(st: String): Map<String, String> {
        val str = st.replace("\"", "")
        val dict: HashMap<String, String> = HashMap()

        val res: List<String> = str.split("&")
        for (i in res){
            dict.put(i.split("=")[0], i.split("=")[1])
        }
        val urlAdress = "https://proverkacheka.nalog.ru:9999/v1/inns/*/kkts/*/fss/" +
                "${dict.get("fn")}/tickets/${dict.get("i")}?fiscalSign=${dict.get("fp")}&sendToEmail=no"

        val summa = dict.get("s").toString().replace(".", "")

        val dateOf = rightDate(dict.get("t").toString())

        val urlAdresstTry = "https://proverkacheka.nalog.ru:9999/v1/ofds/*/inns/*/fss/" +
                "${dict.get("fn")}/operations/1/tickets/${dict.get("i")}?fiscalSign=${dict.get("fp")}" +
                "&date=${dateOf}&sum=${summa}"

        return mapOf<String, String>(
                URL_TRY to urlAdresstTry,
                URL_CHEQ to urlAdress,
                SUM to summa,
                DATE to dateOf
        )
    }

    /**
     * There methods for building resyclers
     */


    private fun createCheqRecycle(
            json: ArrayList<Any?>,
            isOwner: Boolean
    ) {

        with(listRes) {
            setHasFixedSize(true)
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@CheqActivity)
            adapter = CheqAdapter(json, isOwner)
        }
    }
/**
    private fun createBallsAndGiftRecycle(
            jsonBalls: JsonArray?,
            jsonGift: JsonArray?
    ) {

        with(ballsGiftResycler) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@CheqActivity)
            adapter = BallsAndGiftsResycler(jsonBalls!!, jsonGift)
        }
    }
*/
}


