package ru.cheqin.cheqapp.cheq

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.util.Log
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import org.jetbrains.anko.toast
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.ArrayList

class CheqPresenter(override val kodein: Kodein) : KodeinAware, CheqContract.Presenter {

    val realm: Realm by instance()
    val getCheqer: CheqModel by instance()
    var view: CheqContract.View? = null
    val dataForShow = ArrayList<Any?>()

    var cheqHat: CheqItemHat? = null
    var cheqBody: CheqData? = null
    var cheqInfo: CheqInfo? = null
    var cheqItems: JsonArray? = null

    lateinit var qrScan: String
    lateinit var phone: String
    var locationData: Location? = null


    @SuppressLint("CheckResult", "SimpleDateFormat")
    override fun buildCheq(qr: String, phoneNumber: String, location: Location?, url: String) {
        qrScan = qr
        phone = phoneNumber
        locationData = location
        /**
        val cheq = getCheqer.getCheq(
        qrScan,
        phone,
        url,
        locationData.longitude.toString(),
        locationData.latitude.toString())

        Log.d("CHEQ_DATA", cheq.toString())

        cheq!!.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
        { result -> buildResult(result) },
        { error -> errorTracer(error)}
        )
         **/
        val long: String
        val lati: String
        if (locationData == null){
            long = "0.0"
            lati = "0.0"
        } else {
            long = locationData!!.longitude.toString()
            lati = locationData!!.latitude.toString()
        }


        val cheq = getCheqer.getCheqFuel(
                qrScan,
                phone,
                url,
                long,
                lati
        )

        if (cheq != null) {
            cheq.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { result, err ->
                        Log.d("CHEQ_LOG", "--------->      ${result.first.statusCode}")
                        Log.d("CHEQ_LOG", "--------->      ${result.second.component2()}")
                        Log.d("CHEQ_LOG", "--------->      ${result.second.component1()}")
                        when (result.second) {
                            is Result.Success -> {

                                /**
                                 * START PARSE DATA
                                 */
                                val gson = Gson()
                                val jsonAllData = gson.fromJson<JsonObject>(result.second.component1()!!)
                                if (!jsonAllData.has("error")) {
                                    parseAndCreateDataCheq(jsonAllData)
                                } else {
                                    val activityThis = view as CheqView
                                    activityThis.toast("Возникла проблема в соединении с сервером")
                                    activityThis.finish()
                                }
                            }
                            is Result.Failure -> {
                            }
                        }

                    }
        } else {

        }

    }

    @SuppressLint("SimpleDateFormat")
    private fun parseAndCreateDataCheq(jsonAllData: JsonObject) {
        val jsonResult = jsonAllData[_result].asJsonArray[0].asJsonObject
        val jsonOrganization = jsonResult[_organization].asJsonObject
        val jsonReceipt = jsonResult[_document].asJsonObject[_receipt].asJsonObject


        val jsonIcon = if (jsonOrganization[_tm_url].asString.isNotEmpty())
            jsonOrganization[_tm_url].asString else jsonOrganization[_short_name].asString

        val bonus = jsonResult[_bill_bon_vall].asJsonArray.size() > 0
        val sumString = "${"%.2f"
                .format(jsonReceipt[_totalSum].asInt.toDouble() / 100)
                .replace(',', '.')}\u20BD"

        val status = stringPutterFromJson(jsonResult, _status)

        buildFromWeb(
                jsonReceipt,
                jsonIcon,
                bonus, status,
                jsonOrganization,
                jsonResult[_im_owner].asBoolean,
                sumString
        )
    }


    private fun saveToBase(jsonReceipt: JsonObject){

    }


    private fun buildFromWeb(
            jsonReceipt: JsonObject,
            jsonIcon: String?,
            bonus: Boolean, status: String?,
            jsonOrganization: JsonObject,
            isOwner: Boolean,
            sumString: String
    ) {
        cheqItems = jsonReceipt[_items].asJsonArray

        /**
         * data class CheqItemHat(
        var icon:String? = null,
        var name:String? = null,
        var inn:String? = null,
        var addr:String? = null,
        var date: Date? = null,
        var time: String? =null

        )
         */
        val (date, time) = jsonReceipt[_dateTime]
                .asString
                .split("T")
        val dateObject = SimpleDateFormat("yyyy-MM-dd").parse(date)

        cheqHat = CheqItemHat()
        with(cheqHat!!) {
            icon = jsonIcon
            name = stringPutterFromJson(jsonReceipt, _user)
            inn = stringPutterFromJson(jsonReceipt, _userInn)
            addr = stringPutterFromJson(jsonReceipt, _address)
            this.date = dateObject
            this.time = time
        }

        /**
         * data class CheqData(
        val id: String,
        val bonus: Boolean=false,
        val sum: String,
        val date_time: String?=null,
        val unixTime: Long?=null,
        val isGetted: Boolean=false,
        val organization: String?=null,
        val short_name: String?=null,
        val iconOrg: String?=null
        )
         */
        // TODO: Refactoring!!!
        cheqBody = CheqData()

        with(cheqBody!!) {
            this.id = qrScan
            this.bonus = bonus

            sum = "%.2f".format(jsonReceipt[_totalSum].asInt.toDouble() / 100)
            date_time = jsonReceipt[_dateTime].asString
            unixTime = dateObject.time
            if(status != null) {
                when(status){
                    IsGetted.success.toString() -> isGetted = true
                    IsGetted.notFound.toString() -> notFound = true
                }
            }
            organization = jsonOrganization[_name].asString
            short_name = jsonOrganization[_short_name].asString
            iconOrg = jsonIcon


        }


        /**
         * data class CheqInfo(
        var nalog: String? = null,
        var kkt: String? = null,
        var fn: String? = null,
        var fd: String? = null,
        var fpd: String? = null,
        var billNumber:String? = null,
        var cashMen:String? = null,
        var groupNumber:String? = null
        )
         */

        cheqInfo = CheqInfo()
        with(cheqInfo!!) {
            kkt = stringPutterFromJson(jsonReceipt, _kktRegId)?.trim()
            cashMen = stringPutterFromJson(jsonReceipt, _operator)
            nalog = intPutterFromJson(jsonReceipt, _taxationType).toString()
            fd = intPutterFromJson(jsonReceipt, _fiscalDocumentNumber).toString()
            fn = stringPutterFromJson(jsonReceipt, _fiscalDriveNumber)
            fpd = intPutterFromJson(jsonReceipt, _fiscalSign).toString().replace("-", "")
        }


        dataForShow.add(cheqBody)

        if (status == IsGetted.success.toString()) {
            if (jsonReceipt.has(_cheq_gift) && isOwner) {
                val jsonGift = jsonReceipt[_cheq_gift].asJsonArray
                val gift = CheqGift(
                        jsonGift[0].asString,
                        jsonGift[1].asString,
                        jsonGift[2].asString,
                        jsonGift[3].asString
//                        jsonGift[6].asString
                )
                if (jsonGift.size() > 6) {
                    gift.code = jsonGift[6].asString
                }
                if (jsonGift.size() > 7) {
                    gift.code = jsonGift[7].asString
                    gift.isSecret = jsonGift[8].asBoolean
                }

                dataForShow.add(gift)
            }
            dataForShow.add(null)
            dataForShow.add(cheqHat)
            dataForShow.add("dot")

            var itemNumber = 0
            for (i in cheqItems!!) {
                itemNumber++
                dataForShow.add(CheqItemAndNumber(itemNumber, i))
            }
            dataForShow.add("dot")

            dataForShow.add(
                    Summa(sumString, "Итого")
            )
            dataForShow.add(null)
            dataForShow.add(cheqInfo)
            dataForShow.add(null)
            dataForShow.add(QRString(qrScan))
        }

        view!!.showCheq(dataForShow, isOwner)
    }

    fun buildResult(cheqData: Cheq) {
        Log.d("CHEQ_DATA", cheqData.toString())
    }

    fun errorTracer(error: Throwable) {
        error.printStackTrace()
    }


    private fun intPutterFromJson( jsonReceipt: JsonObject, key:String): Int? {
        return if (jsonReceipt.has(key))
            try {

                jsonReceipt[key].asInt
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        else null
    }

    private fun stringPutterFromJson(jsonReceipt: JsonObject, key:String): String? {
        return if (jsonReceipt.has(key))
            try {
                jsonReceipt[key].asString
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        else null
    }



    override fun attach(context: CheqContract.View) {
        view = context
    }

    override fun detach() {
        view = null
    }

    companion object {
        const val id = "id"

        const val _result = "result"
        const val _im_owner = "im_owner"
        const val _organization = "organization"
        const val _tm_url = "tm_url"
        const val _name = "name"
        const val _short_name = "short_name"
        const val _cheq_main_bonus = "cheq_main_bonus"
        const val _scan_time = "scan_time"
        const val _status = "status"
        const val _document = "document"
        const val _receipt = "receipt"
        const val _cashTotalSum = "cashTotalSum"
        const val _kktRegId = "kktRegId"
        const val _fiscalDocumentNumber = "fiscalDocumentNumber"
        const val _operationType = "operationType"
        const val _fiscalDriveNumber = "fiscalDriveNumber"
        const val _fiscalSign = "fiscalSign"
        const val _shiftNumber = "shiftNumber"
        const val _userInn = "userInn"
        const val _dateTime = "dateTime"
        const val _receiptCode = "receiptCode"
        const val _items = "items"
        const val _address = "address"
        const val _requestNumber = "requestNumber"
        const val _nds18 = "nds18"
        const val _retailPlaceAddress = "retailPlaceAddress"
        const val _totalSum = "totalSum"
        const val _taxationType = "taxationType"
        const val _operator = "operator"
        const val _user = "user"
        const val _ecashTotalSum = "ecashTotalSum"
        const val _bill_bon_vall = "bill_bon_val"
        const val _cheq_gift = "cheq_gift"

    }


}

/**
{
"id": 1,
"result": [
{
"im_owner": false,
"organization": {
"tm_url": "",
"name": "ООО \"САНТОРГ\"",
"short_name": "С"
},
"cheq_main_bonus": 2085,
"scan_time": 0,
"status": "success",
"document": {
"receipt": {
"cashTotalSum": 0,
"kktRegId": "0000467170023480    ",
"fiscalDocumentNumber": 47502,
"operationType": 1,
"fiscalDriveNumber": "8710000101778478",
"fiscalSign": 1393234272,
"shiftNumber": 56,
"userInn": "7715158207",
"dateTime": "2018-05-21T14:01:00",
"receiptCode": 3,
"items": [
{
"sum": 3900,
"cheq_id": 2115,
"quantity": 1,
"name": "НАПИТОК СПРАЙТ 0,33л",
"price": 3900
},
{
"sum": 3900,
"cheq_id": 2116,
"quantity": 1,
"name": "НАПИТОК СПРАЙТ 0,33л",
"price": 3900
},
{
"sum": 18500,
"cheq_id": 2117,
"quantity": 1,
"name": "КОФЕ Жокей молотый 250г асс",
"price": 18500
},
{
"sum": 15400,
"cheq_id": 2118,
"quantity": 1,
"name": "КОФЕ ЖОКЕЙ 250г традицион.молотый",
"price": 15400
}
],
"requestNumber": 437,
"nds18": 6361,
"retailPlaceAddress": "105318, г. Москва, Семеновская пл., дом 1",
"totalSum": 41700,
"taxationType": 1,
"operator": "Ченская",
"user": "ООО \"САНТОРГ\"",
"ecashTotalSum": 41700
}
},
"bill_bon_val": []
}
]
}
 */