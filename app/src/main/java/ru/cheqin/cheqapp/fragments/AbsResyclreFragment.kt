package ru.cheqin.cheqapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.orhanobut.hawk.Hawk
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.cheqin.cheqapp.actions.ActionsAdapter
import ru.cheqin.cheqapp.tools.SERVER
import ru.cheqin.cheqapp.tools.SERVERD

abstract class AbsResyclerFragment: android.app.Fragment() {

    lateinit var layoutManagerRecycler: androidx.recyclerview.widget.RecyclerView.LayoutManager
    lateinit var adapterActions: ActionsAdapter
    var url: String = SERVER



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    open fun createRecycler(view: View, data: JsonArray, resyclerId: Int, isOfline:Boolean=false) {
        layoutManagerRecycler = androidx.recyclerview.widget.LinearLayoutManager(view.context)
        adapterActions = ActionsAdapter(data, isOfline)


        view.findViewById<androidx.recyclerview.widget.RecyclerView>(resyclerId).apply {
            setHasFixedSize(true)
            layoutManager = layoutManagerRecycler
            adapter = adapterActions
        }
    }

    open fun postRxToServer(data: JsonObject): Single<Pair<Response, Result<String, FuelError>>>? {
        Log.d("JSON__DATA", data.toString())
        return url.httpPost().body(data.toString()).rx_responseString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
    }

}
