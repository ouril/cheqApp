package ru.cheqin.cheqapp.actions

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.orhanobut.hawk.Hawk
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.one_action_activity.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import ru.cheqin.cheqapp.FullDescriptionPromo
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.*
import ru.cheqin.cheqapp.scaner.ScannerActivity
import java.text.SimpleDateFormat
import java.util.*



class OneActionActivity: AppCompatActivity() {
    lateinit var url: String


    @SuppressLint("SetTextI18n", "CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_action_activity)
        scanButton.setOnClickListener {
            startActivity<ScannerActivity>()
            finish()
        }
        val id = intent.extras.getString(ID)
        Hawk.init(this).build()
        url = if (Hawk.get("IS_PROD")) SERVER else SERVERD
        back.setOnClickListener {
            finish()
        }


        promp_full_description.setOnClickListener {
            startActivity<FullDescriptionPromo>(ID to id)
        }

        getPromFromServerById(id)!!.subscribe { pair, _ ->
            val (response, result) = pair
            if (response.statusCode in 0..399) {

                try {

                    val json = result.component1().toString()
                    val gson = Gson()
                    val res = gson.fromJson<JsonObject>(json).get("result").asJsonArray[0].asJsonArray
                    println(res.toString())
                    val width = actionCard.context.resources.displayMetrics.widthPixels
                    loadImageIntoItemForOneAction(res[5].asString, imageAction, width)
                    nameAction.setText(res[1].asString)
                    aboutAction.setText(res[4].asString)
                    textView5.setText(res[9].asString)
                    ballsAction.setText("${res[7].asInt}")

                    val startTime = getDateTimeFromUnixWithMonthName(res[2].asString)
                    val endTime = getDateTimeFromUnixWithMonthName(res[3].asString)
                    println("$startTime - $endTime")

                    dateAction.setText("$startTime - $endTime")

                } catch (e: Exception) {
                    toast("Видимо эта акция уже закончилась")
                    e.printStackTrace()
                }


            } else {
                toast("Проверьте соединение с интернетом")
                finish()
            }
        }

    }

    private fun getPromFromServerById(id: String): Single<Pair<Response, Result<String, FuelError>>>?  {

        val json = jsonObject(
                "method" to "bill_api.$PROMO_ID",
                "params" to jsonArray(id.toInt()),
                "id" to id.toInt()
        )

        return url.httpPost().body(json.toString()).rx_responseString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())

    }


    @SuppressLint("SimpleDateFormat")
    private fun getDateTime(s: String): String {
        try {
            val unixTime = java.lang.Long.parseLong(s)
            val time = java.util.Date(unixTime * 1000)
            val sdf: SimpleDateFormat
            sdf = SimpleDateFormat("dd.MM.yy")
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+3"))
            return sdf.format(time)
        } catch (e: Exception) {
            return e.toString()
        }

    }



}