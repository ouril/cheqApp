package ru.cheqin.cheqapp.actions

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.action_item.view.*
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.ID
import ru.cheqin.cheqapp.tools.getDateTimeFromUnixWithMonthName
import ru.cheqin.cheqapp.tools.loadImageIntoItem

class ActionsAdapter internal constructor(
        val dataSet: JsonArray,
        val isOfline: Boolean = false
) : androidx.recyclerview.widget.RecyclerView.Adapter<ActionsAdapter.CustomViewHolder>() {

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val obj = dataSet.get(position).asJsonArray
        val width = holder.itemView.context.resources.displayMetrics.widthPixels

        holder.aboutAction.text = obj[4].asString
        val str = obj[1].asString

        holder.nameAction.text = str

        loadImageIntoItem(obj[5].asString, holder.imageAction, width, isOfline)
        holder.timeAction.text = "${getDateTimeFromUnixWithMonthName(obj[2].asString)} - ${getDateTimeFromUnixWithMonthName(obj[3].asString)}"
        holder.score.text = obj[7].asString

        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, OneActionActivity::class.java)
            intent.putExtra(ID, obj[0].asString)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CustomViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.action_item, parent, false)

        return CustomViewHolder(v)
    }

    class CustomViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v), View.OnClickListener {

        val score = v.bonusScore!!
        val nameAction = v.nameAction!!
        val aboutAction = v.shot_about!!
        val timeAction = v.action_delta_value!!
        val imageAction = v.action_image_big!!
        //val nameActionTop = v.nameActionTop!!

        override fun onClick(v: View) {}
    }
}

