
package ru.cheqin.cheqapp.actions

import android.annotation.SuppressLint
import android.app.Fragment
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.get
import com.github.salomonbrys.kotson.jsonArray
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.realm.Realm
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.dataBaseUtils.ActionsModel
import ru.cheqin.cheqapp.tools.BILL_LIST
import ru.cheqin.cheqapp.tools.jsonListBuilder
import java.io.File
import java.net.URL
import android.graphics.Bitmap
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.util.Log
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.rx.rx_responseString
import com.github.kittinunf.result.Result
import com.orhanobut.hawk.Hawk
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_actipons.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import java.io.FileOutputStream
import java.lang.Exception
import kotlinx.coroutines.*
import ru.cheqin.cheqapp.fragments.AbsResyclerFragment
import ru.cheqin.cheqapp.tools.SERVER
import ru.cheqin.cheqapp.tools.SERVERD


class ActionsFragment : AbsResyclerFragment(), KodeinAware {
    override val kodein by closestKodein()
    val realm: Realm by  instance()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Hawk.init(this.activity).build()
        url = if (Hawk.get("IS_PROD")) SERVER else SERVERD

        val view = inflater!!.inflate(R.layout.fragment_actipons, container, false)
        getJsonFromRealm(realm, view)

        getActions(view)
        return view
    }



    override fun postRxToServer(data: JsonObject): Single<Pair<Response, Result<String, FuelError>>>? {
        Log.d("JSON__DATA", data.toString())
        return url.httpPost().body(data.toString()).rx_responseString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
    }


    @SuppressLint("CheckResult")
    private fun getActions(view: View) {
        postRxToServer(jsonListBuilder(BILL_LIST))!!
                .subscribe { pair, _ ->
                    val (response, result) = pair
                    if (response.statusCode in 0..399) {

                        val json = result.component1().toString()
                        val gson = Gson()
                        val res = gson.fromJson<JsonObject>(json)

                        if (res.has("error")) {
                            getJsonFromRealm(realm, view)
                        } else {
                            val dataArray = res["result"][0].asJsonArray
                            createRecycler(view, dataArray, R.id.actions_recycler)
                            createOrUpdateActions(realm, dataArray)
                        }
                    }


                }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val refresh =
                this.activity.findViewById<androidx.swiperefreshlayout.widget.SwipeRefreshLayout>(R.id.refreshActions)
                as androidx.swiperefreshlayout.widget.SwipeRefreshLayout
        refresh.setOnRefreshListener(
                object : androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

            override fun onRefresh() {
                refreshActions.isRefreshing = true
                getActions(view!!)
                refresh.isRefreshing = false

            }
        })
        super.onViewCreated(view, savedInstanceState)
    }

    private fun getJsonFromRealm(realm: Realm, view: View) {
        val data = realm.where(ActionsModel::class.java).findAll()
        val jsonData = jsonArray()
        data.forEach {
            jsonData.add(
                    jsonArray(
                            it.id,
                            it.name,
                            it.start,
                            it.end,
                            it.description,
                            it.picture,
                            "",
                            it.bonus,
                            "",
                            it.slug
                    )
            )
        }
        createRecycler(view, jsonData, R.id.actions_recycler, true)
    }

    private fun createOrUpdateActions(realm: Realm, dataArray: JsonArray) {
        val allObjInRealm = realm.where(ActionsModel::class.java).findAll()
        val listId: List<Int> = allObjInRealm.map { it.id }
        val listIdFromResponse = dataArray.map { it[0].asInt }
        val deleteList = listId.filter { it !in listIdFromResponse }

        deleteList.forEach {
            val objForDel = realm.where(ActionsModel::class.java).equalTo("id", it).findFirst()
            realm.beginTransaction()
            objForDel!!.deleteFromRealm()
            realm.commitTransaction()
        }

        dataArray.forEach {
            realm.beginTransaction()
            val itId = it[0].asInt
            val picUrl = it[5].asString
            val fileSplitParts: List<String> = picUrl.split('/')
            val nameFile = fileSplitParts[fileSplitParts.size - 1]
            val filePath = "${activity.cacheDir.absolutePath}/$nameFile"
            val action = if (itId !in listId) realm.createObject(ActionsModel::class.java, itId)
                else allObjInRealm.filter { it.id == itId }[0]

            action!!.name = it[1].asString
            action.start = it[2].asInt
            action.end = it[3].asInt
            action.description = it[4].asString
            action.picture = filePath
            action.bonus = it[7].asInt
            action.slug = it[9].asString
            realm.commitTransaction()

            GlobalScope.launch {
                    try {
                        val url = URL(picUrl)
                        val urlConection = url.openConnection()
                        val bitmap = BitmapFactory.decodeStream(urlConection.getInputStream())
                        val f = File(filePath)
                        val fOut = FileOutputStream(f)
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
                        fOut.flush()
                        fOut.close()
                    } catch (e: Exception) {
                        Log.d("DOWNLOAD_ERROR", e.toString())
                    }
            }
        }
    }


    companion object {
        fun newInstance(): Fragment {
            return ActionsFragment()
        }
    }
}

