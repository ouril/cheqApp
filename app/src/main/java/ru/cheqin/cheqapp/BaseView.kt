package ru.cheqin.cheqapp

import android.content.Context
import com.orhanobut.hawk.Hawk

interface BaseView {

    fun getHawk()

    fun getContext(): Context

}