package ru.cheqin.cheqapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.user_perm.*


class DocumentActivity: AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_perm)
        floatingCancel.setOnClickListener {
            finish()
        }
    }
}