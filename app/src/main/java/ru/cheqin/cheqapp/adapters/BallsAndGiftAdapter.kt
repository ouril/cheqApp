package ru.cheqin.cheqapp.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.gift_bonus_item.view.*
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.loadImage


class BallsAndGiftsResycler(val myDataset: JsonArray, val gift:JsonArray? = null) :
        androidx.recyclerview.widget.RecyclerView.Adapter<BallsAndGiftsResycler.ViewHolder>() {

    inner class ViewHolder(val v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v),
            View.OnClickListener {

        val imageGift = v.giftPic as ImageView
        val imageBall = v.imageAction as ImageView
        val ballScore = v.actionScore as TextView
        val giftName = v.giftText as TextView
        val giftCard = v.giftLayout as View

        override fun onClick(v: View?) {
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.gift_bonus_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = myDataset[position].asJsonObject
        if (position == 0 && gift != null){

            holder.giftCard.visibility = View.VISIBLE
            loadImage(gift[3].toString(), holder.imageGift)
            holder.giftName.setText("Ваш приз ${gift[2]}")
        }

        loadImage(data["link"].asString, holder.imageBall)
        val value = data["value"].asLong / 100
        holder.ballScore.setText("$value")

    }

    override fun getItemCount() = myDataset.size()

}