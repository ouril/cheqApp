package ru.cheqin.cheqapp.registration

import android.annotation.SuppressLint
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.orhanobut.hawk.Hawk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import ru.cheqin.cheqapp.profile.Answer
import ru.cheqin.cheqapp.profile.ProfileModel
import ru.cheqin.cheqapp.tools.*
import java.lang.Exception

class GetPassPresenter: RegistrationContract.GetPassPresenter {

    val profileModel: ProfileModel = ProfileModel()
    var context: AppCompatActivity? = null
    val url: String

    init {
        url = if(Hawk.get("IS_PROD")) SERVER_RET else SERVER_DEV
    }



    @SuppressLint("CheckResult")
    override fun verefiCode(phoneNumber: String, code: String, name: String, mail: String) {
        val res = profileModel.verefyCode(phoneNumber, code, url)
        res.subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        { result -> serveAnswer(result, name, phoneNumber, mail)},
                        { error -> error.printStackTrace() }
                )
    }

    private fun serveAnswer(result: Answer, name: String, phoneNumber: String, mail: String) {
        try {
            if (result.error == null) {
                Hawk.put(NAME, name)
                Hawk.put(PHONE, phoneNumber)
                Hawk.put(EMAIL, mail)
                Hawk.put(ISREG, true)
                val token = Hawk.get<String>("FIREBASE_TOKEN")


                val json = jsonObject(
                        "method" to "bill_api.set_firebase_token",
                        "params" to jsonArray(
                                token, phoneNumber
                        ),
                        "id" to 1

                )
                println("json")
                url.httpPost()
                        .body(json.toString())
                        .responseJson { request, response, resultS ->
                            println(request)
                            println(result)
                            when(resultS){
                                is Result.Success -> {
                                    //Hawk.put("FIREBASE_TOKEN", token)
                                    Log.d("REG_IS_OK", "---------------> $token")
                                }
                                is Result.Failure -> {

                                    Log.d("REG_IS_NOT_OK", "---------------> ${result.error}")
                                    //TODO: Add new way to result when it's do mistake
                                }
                            }
                        }


                with(context!!){
                    toast("Регистрация прошла успешно!")
                    finish()
                }

            } else {
                with(context!!) {
                    startActivity<NewRegistrationActivity>()
                    toast("Введен невеный код!")
                    finish()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @SuppressLint("CheckResult")
    override fun reSend(uuid: String, phoneNumber: String, mail: String) {
        val res = profileModel.inputNumber(uuid, phoneNumber, url)
        res.subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        {result -> serveAnswerResend(result, mail, uuid)},
                        {error -> serveErrorResend(error, mail, uuid)}
                )
    }

    fun serveErrorResend(e: Throwable, mail: String, uuid: String){
        e.printStackTrace()
        with(context!!){
            startActivity<GetPassActivity>(
                    EMAIL to mail,
                    "UUID" to uuid
            )
            toast("Попробуйте еще раз!")
            finish()
        }


    }

    fun serveAnswerResend(a: Answer, mail: String, uuid: String){
        if (a.error == null){
            with(context!!){




                startActivity<GetPassActivity>(
                        EMAIL to mail,
                        "UUID" to uuid
                )
                toast("Выслан новый код!")
                finish()
            }
        }
    }

    override fun detach() {
        context = null
    }

    override fun attach(context: AppCompatActivity) {
        this.context = context
    }
}