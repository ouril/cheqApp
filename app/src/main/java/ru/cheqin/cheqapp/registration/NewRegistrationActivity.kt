package ru.cheqin.cheqapp.registration

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.orhanobut.hawk.Hawk
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import kotlinx.android.synthetic.main.first_registration_fragment.*
import kotlinx.android.synthetic.main.menu_fragment.*
import org.jetbrains.anko.startActivity
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import ru.cheqin.cheqapp.PPActivity
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.NAME
import ru.cheqin.cheqapp.tools.PHONE
import ru.cheqin.cheqapp.tools.phoneStringStrip
import ru.cheqin.cheqapp.tools.phoneVaslidator

class NewRegistrationActivity: AppCompatActivity(), KodeinAware, RegistrationContract.View{
    override val kodein: Kodein by closestKodein()

    val REG_PROBLEM = "REG_PROBLEMS"
    var regLogic: RegistrationContract.RegistrationPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.first_registration_fragment)
        regLogic = RegistrationLogic(this, kodein)
        regLogic!!.takeFromCatch()
        editText.setText(regLogic!!.nameUser)
        editText2.setText(regLogic!!.phone)

        licenseText.setOnClickListener {
            startActivity<PPActivity>("IS_POLICE" to false)
        }

        licenseText2.setOnClickListener {
            startActivity<PPActivity>("IS_POLICE" to true)
        }


        requestCameraPermission()
        getListners(editText, RuTextFieldEngine())
        getListners(editText2, PhoneFieldEngine())

        button.setOnClickListener {

            val phoneFromField = editText2.text.toString()
            val nameFromField = editText.text.toString()
            println("${phoneFromField.phoneStringStrip()} ---------------------------- $nameFromField")
            if (
                    editText.text.isNotBlank()
                    && phoneVaslidator(phoneFromField.phoneStringStrip())
                    && phoneFromField.phoneStringStrip().length == 12
                    && checkBox.isChecked
            ) {


                with(regLogic!!) {
                    nameUser = nameFromField
                    phone = phoneFromField
                    saveToCache(nameFromField, phoneFromField)
                }

                regLogic!!.toRegistrate()
                button.isEnabled = false

            } else {

                    if (!(phoneFromField.phoneStringStrip().length < 12) &&
                            !phoneVaslidator(phoneFromField.phoneStringStrip()))
                        showDialog("Введён неверно телефон")
                    else if (!checkBox.isChecked)
                        showDialog("Вы согласны с лицензионным соглашением?")
                    else if (!editText.text.isNotBlank())
                        showDialog("Введите имя")

            }

        }
/*
        Observables.combineLatest(editTextNameObserver, editTextPhoneObserver).subscribe {
            println("${it.first} --- ${it.second}")
            if (
                    it.first.isNotBlank()
                    && phoneVaslidator(it.second)
                    && checkBox.isChecked
            ) {
                println("bagggggg!!!!!!!!!!!")
                with(regLogic) {
                    nameUser = it.first
                    phone = it.second
                    saveToCache(it.first, it.second)
                }
                button.setOnClickListener {
                    regLogic.toRegistrate()
                }
            }

/*
            else {
                button.setOnClickListener {
                    if(phoneVaslidator(editText2.text.toString()))
                        showDialog("Введён неверно телефон")
                    else if(!checkBox.isChecked)
                        showDialog("Поставьте галочку")
                }

            }
*/
            }

*/
        }



        fun showDialog(text: String) {
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.grey_dialog)
            val dialogText = dialog.findViewById<TextView>(R.id.dialogText)
            dialogText.text = text
            val dialogButton = dialog.findViewById<Button>(R.id.dialog_button)
            dialogButton.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }



    fun getListners(editTextView: EditText, fieldEngine: FieldEngine) {//: Observable<String> {

        //val observale = Observable.create<String> {
            //val textChangedEngin: FieldEngine = fieldEngine
            editTextView.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    //it.onNext(s.toString())

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    println(s)
                    fieldEngine.formatText(editTextView, s)
                }

                override fun afterTextChanged(s: Editable) {
                    //it.onNext(s.toString())

                }
            })
        }
        //return observale


    private fun requestCameraPermission() {
        val permissions = arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, 2)
        }
    }

    override fun onDestroy() {
        regLogic!!.detach()
        super.onDestroy()
    }



}