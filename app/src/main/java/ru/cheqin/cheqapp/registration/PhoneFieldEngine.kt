package ru.cheqin.cheqapp.registration

import android.widget.EditText
import java.lang.Exception

class PhoneFieldEngine: FieldEngine {
    override fun formatText(v: EditText, s: CharSequence) {
        try {
            if (s!![0] == '8' || s!![0] == '7' || s!![0] !== '+') {

                var str: String = s.toString()
                val newString = "+7" + str.substring(1)
                v.setText(newString)
                v.setSelection(v.text.length)

            }
        } catch (e:Exception) {
            e.printStackTrace()
        }
        setRightSymbol(s, 2, "(", v)
        setRightSymbol(s, 6, ")", v)
        setRightSymbol(s, 10, "-", v)
    }

    fun setRightSymbol(s: CharSequence, position: Int, symbol: String, v: EditText) {
        if (s.length == (position + 1) && !(symbol in s)) {
            val codeWithBrakets = "${s.substring(0, position)}$symbol${s[position]}"
            v.setText(codeWithBrakets)
            v.setSelection(position + 2)
        }
    }

}