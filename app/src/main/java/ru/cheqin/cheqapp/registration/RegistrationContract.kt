package ru.cheqin.cheqapp.registration

import android.content.Context
import androidx.appcompat.app.AppCompatActivity

interface RegistrationContract {
    interface View {

    }

    interface GetPassPresenter {

        fun verefiCode(phoneNumber: String, code: String, name: String, mail: String)

        fun reSend(uuid:String, phoneNumber: String, mail: String)


        fun attach(context: AppCompatActivity)


        fun detach()

    }


    interface RegistrationPresenter {
        var statusCode: Int
        var statusMessage: String
        var nameUser: String
        var email: String
        var phone: String

        fun toRegistrate()

        fun detach()

        fun saveToCache(v1: String, v2: String)

        fun takeFromCatch()
    }
}