package ru.cheqin.cheqapp.registration

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.Observable
import kotlinx.android.synthetic.main.second_registration_fragment.*
import java.util.Timer
import java.util.TimerTask
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.orhanobut.hawk.Hawk
import io.reactivex.rxkotlin.Observables.combineLatest
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.tools.*


class GetPassActivity : AppCompatActivity(), KodeinAware {
    override val kodein: Kodein by instance()
    lateinit var name: String
    lateinit var uuid: String
    lateinit var email: String
    lateinit var phoneNumber: String
    lateinit var reglog: RegistrationContract.GetPassPresenter

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_registration_fragment)
        reglog = GetPassPresenter()
        reglog.attach(this)
        Hawk.init(this).build()
        phoneNumber = Hawk.get(PHONE_CACHE, "").phoneStringStrip()
        name = Hawk.get(NAME_CACHE, "")
        uuid = intent.extras.getString("UUID")
        email = intent.extras.getString(EMAIL)
        cancel.setOnClickListener {
            finish()
        }

        addListener(editText1, editText2, null, line1)
        addListener(editText2, editText3, editText1, line2)
        addListener(editText3, editText4, editText2, line3)
        addListener(editText4, editText5, editText3, line4)
        addListener(editText5, editText6, editText4, line5)
        addListener(editText6, null, editText5, line6)

        val observer1 = addObserver(editText1)

        val observer2 = addObserver(editText2)
        val observer3 = addObserver(editText3)
        val observer4 = addObserver(editText4)
        val observer5 = addObserver(editText5)
        val observer6 = addObserver(editText6)

        val cF = {
            s1:String, s2:String, s3:String, s4:String, s5:String, s6:String -> s1 + s2 + s3 + s4 + s5 + s6
        }


        combineLatest(observer1, observer2, observer3, observer4, observer5, observer6, cF).subscribe {
            /*
            PROVERKA_LOGIN.httpGet()
                    .authenticate(phoneNumber, it)
                    .header(headersForRegistration())
                    .response { request, response, result ->
                        val LOGIN_RESULT = "LOGIN_RESULT"
                        Log.d(LOGIN_RESULT, request.toString())
                        Log.d(LOGIN_RESULT, response.toString())

                        when(result) {
                            is Result.Success -> {
                                Hawk.put(PHONE, phoneNumber)
                                Hawk.put(PASSWORD, it)
                                Hawk.put(EMAIL, emailBuilder(phoneNumber))
                                Hawk.put(NAME, name)
                                toast("Регистрация прошла успешно!")
                                finish()
                            }
                            is Result.Failure -> {
                                showDialog("Введён невеный код")


                            }
                        }
                    }
                    */
            reglog.verefiCode(phoneNumber, it, name, email!!)

        }
        phoneText.text = phoneNumber



        timerStart()
        val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboard.showSoftInput(editText1, 0)
    }


    fun timerStart(){
        var time = 30

        val timer = Timer()
        val timerTask = object : TimerTask() {

            override fun run() {
                runOnUiThread {
                    time -=1
                    println(time)
                    when (time){
                        in 1..9 -> timerView.text = "00:0$time сек"
                        in 10..30 -> timerView.text = "00:$time сек"
                        0 -> {
                            timerView.text = "00:0$time сек"
                            timerView.visibility = View.INVISIBLE
                            with(timerText) {
                                text = "Оправить код повторно"
                                setOnClickListener {
                                    reglog.reSend(
                                            uuid,
                                            phoneNumber,
                                            this@GetPassActivity.email
                                    )
/*

                                    with(reglog) {
                                        nameUser = name
                                        phone = phoneNumber
                                        toRegistrate()
                                    }
*/                                }
                            }
                            cancel()
                            }
                        }
                    }

                    }

            }

        timer.scheduleAtFixedRate(timerTask, 1000, 1000)

    }

    fun addObserver(
            editTextView: EditText
    ): Observable<String> {
        val observer = Observable.create<String> {
            editTextView.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    println(s)
                    it.onNext(s.toString())


                    }
                }

            )}

        return observer
    }

    fun addListener(
            editTextView: EditText,
            nexTextEdit: EditText?,
            beforText: EditText?,
            v: View
    ) {
            editTextView.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    if (s.isNullOrBlank() && beforText != null) {
                        beforText.requestFocus()
                        v.visibility = View.VISIBLE
                        val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        keyboard.showSoftInput(nexTextEdit, 0)
                    }

                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    if(nexTextEdit != null && count > 0) {
                        v.visibility = View.INVISIBLE
                        nexTextEdit.requestFocus()
                        val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE)
                                as InputMethodManager
                        keyboard.showSoftInput(nexTextEdit, 0)
                    } else if (nexTextEdit == null) {
                        v.visibility = View.INVISIBLE
                        editTextView.isEnabled =false
                    }



                }
            }

            )

    }
    fun showDialog(text: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.grey_dialog)
        val dialogText = dialog.findViewById<TextView>(R.id.dialogText)
        dialogText.text = text
        val dialogButton = dialog.findViewById<Button>(R.id.dialog_button)
        dialogButton.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()

    }

    override fun onDestroy() {
        reglog.detach()
        super.onDestroy()
    }



}



