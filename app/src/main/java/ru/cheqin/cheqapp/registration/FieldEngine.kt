package ru.cheqin.cheqapp.registration

import android.view.View
import android.widget.EditText

interface FieldEngine {
    fun formatText(v: EditText, s:CharSequence)
}