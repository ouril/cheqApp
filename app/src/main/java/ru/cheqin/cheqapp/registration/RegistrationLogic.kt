package ru.cheqin.cheqapp.registration

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.jsonObject
import com.orhanobut.hawk.Hawk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.cheqin.cheqapp.profile.Answer
import ru.cheqin.cheqapp.profile.ProfileModel
import ru.cheqin.cheqapp.tools.*
import java.lang.Exception


class RegistrationLogic(var context: AppCompatActivity?, override val kodein: Kodein) :
        KodeinAware,
        RegistrationContract.RegistrationPresenter{

    private val profileModel: ProfileModel by instance()
    val url: String = if(Hawk.get("IS_PROD")) SERVER_RET else SERVER_DEV


    override var nameUser: String = ""
    override var email: String = ""

    override var statusCode: Int = 0
    override var statusMessage: String = ""

    var uuid: String

    init {
        Hawk.init(context).build()
        uuid = Hawk.get(PHONE)

    }

    override var phone: String = ""
        set(value) {
            if (phoneVaslidator(value)){
                field = value
                        .replace(")", "")
                        .replace("(", "")
                        .replace("-","")
                email = emailBuilder(field)


            } else if(value == ""){
                field = value
            } else {
                throw Exception("Is not phone!!")
            }
        }


    override fun toRegistrate() {
       sendRegistration()

    }

    override fun saveToCache(v1: String, v2: String) {
        Hawk.put(NAME_CACHE, v1)
        Hawk.put(PHONE_CACHE, v2)
        //Hawk.destroy()
    }

    override fun takeFromCatch(){

        nameUser = Hawk.get(NAME_CACHE, "")
        phone = Hawk.get(PHONE_CACHE, "")

    }


    @SuppressLint("CheckResult")
    private fun sendRegistration() {
        val res = profileModel.inputNumber(phone, uuid, url)
        res.subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        result -> getResult(result) },
                            { error -> serveError(error) })
                    }

    private fun serveError(error: Throwable) {
        error.printStackTrace()
        context!!.toast("Что-то пошло не так... Попробуйте еще раз!")
        context!!.startActivity<NewRegistrationActivity>()
        context!!.finish()
    }

    private fun getResult(result: Answer) {
        if (result.error == null) {
            context!!.startActivity<GetPassActivity>(
                    PHONE to phone,
                    NAME to nameUser,
                    EMAIL to email,
                    "UUID" to uuid
            )
            context!!.finish()
        } else {
            context!!.toast("Проверьте правильность ввеенных данных")
            context!!.startActivity<NewRegistrationActivity>()
            context!!.finish()
        }

    }


/*
                .enqueue(object : Callback<Answer> {
            override fun onFailure(call: Call<Answer>, t: Throwable) {
                context.toast("Проверьте соединение с интернетом!")
            }

            override fun onResponse(call: Call<Answer>, response: Response<Answer>) {

                if(response.body()!!.result[0]){
                    context.startActivity<GetPassActivity>()
                    context.finish()
                } else {
                    context.toast("Что-то пошло не так...")
                }
            }

        })
*/

/*
        val json = jsonObject(
                "phone" to phone.phoneStringStrip(),
                "email" to email,
                "name" to nameUser
        )

        val jsonForReset = jsonObject(
                "phone" to phone.phoneStringStrip()
        )

        PROVERKA_SIGN_UP.httpPost()
                .body(json.toString())
                .header(headersForRegistration())
                .response { request, response, result ->
                    when(result){
                        is Result.Success -> {
                            Log.d(REG_LOG, request.toString())
                        }
                        is Result.Failure -> {
                            Log.d(REG_LOG, request.toString())
                            PROVERKA_RESET.httpPost()
                                    .body(jsonForReset.toString())
                                    .header(headersForReset())
                                    .responseString {r, r1, r2 ->
                                        when(r2){
                                            is Result.Success -> {
                                                Log.d(REG_LOG, r.toString())
                                                with(context) {
                                                    startActivity<GetPassActivity>()
                                                    finish()
                                                }
                                            }
                                            is Result.Failure -> {

                                                context.toast("Проверьте соединение!")

                                            }
                                        }


                                    }

                        }
                    }
                }

*/



    override fun detach(){
        context = null
    }


/**
 * на случай если у нас снова воявится ситема логина!
    fun sendLoginRequest(pass:String) : Pair<String, String>  = Pair("name", "email")
*/




}