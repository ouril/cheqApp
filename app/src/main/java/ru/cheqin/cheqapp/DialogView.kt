package ru.cheqin.cheqapp

import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.dialog_registration_layout.*
import org.jetbrains.anko.startActivity
import ru.cheqin.cheqapp.registration.NewRegistrationActivity

class DialogView {
    companion object {
        public fun showDialog(activity: Activity, isFinish: Boolean = false){
            val dialog = Dialog(activity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.dialog_registration_layout)
            val button = dialog.findViewById<Button>(R.id.button2)
            val back = dialog.findViewById<TextView>(R.id.textView777)
            val cheq = dialog.findViewById<ImageView>(R.id.imageView4)
            cheq.setImageResource(R.drawable.ic_cheq)
            button.setOnClickListener {
                activity.startActivity<NewRegistrationActivity>()
                dialog.cancel()
            }
            back.setOnClickListener {

                dialog.cancel()
                if (isFinish) activity.finish()

            }

            dialog.show()
            }


    }
}