package ru.cheqin.cheqapp.main

interface MainContract {

    interface View {
        fun navigate()
    }

    interface Presenter {

    }
}