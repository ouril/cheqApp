package ru.cheqin.cheqapp.main

import android.Manifest
import android.app.Fragment
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import ru.cheqin.cheqapp.actions.ActionsFragment
import ru.cheqin.cheqapp.history.CheqHistoryFragment
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.textColor
import ru.cheqin.cheqapp.BaseView
import ru.cheqin.cheqapp.R
import ru.cheqin.cheqapp.scaner.ScannerActivity
import ru.cheqin.cheqapp.profile.MenuFragment
import ru.cheqin.cheqapp.wallet.WalleyFragment
import java.util.*
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.android.gms.tasks.OnCompleteListener
import org.jetbrains.anko.toast
import ru.cheqin.cheqapp.tools.*
import java.lang.Exception
import io.reactivex.plugins.RxJavaPlugins


class MainActivity : AppCompatActivity(), BaseView {
    override fun getHawk() {
        Hawk.init(this).build()
    }

    override fun getContext(): Context {
        return this //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragment= ActionsFragment.newInstance()
        loadFragment(fragment)

        RxJavaPlugins.setErrorHandler { throwable ->
//            Log.d("RX_ERROR", throwable.localizedMessage)
            throwable.printStackTrace()
        }
        FirebaseMessaging.getInstance().subscribeToTopic("all_android")
        FirebaseMessaging.getInstance().subscribeToTopic("all")

                /**
                .addOnCompleteListener { task ->
                    var msg = getString(R.string.msg_subscribed)
                    if (!task.isSuccessful) {
                        msg = getString(R.string.msg_subscribe_failed)
                    }
                    Log.d("MESSAGE", msg)
                    Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()

                }
                 */

        Hawk.init(this).build()
        Hawk.put("I", 0)

        if(!Hawk.contains(ISREG)){
            Hawk.put(ISREG, false)

        }

        if(!Hawk.contains("IS_PROD")){
            Hawk.put("IS_PROD", true)
        }

        val rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (rc != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission()
        }
        scannerButton.setOnClickListener {
            startActivity<ScannerActivity>()
        }

        actionsButton.setOnClickListener {
            setFragment(0)

        }
        historyButton.setOnClickListener {
           setFragment(1)
        }
        moneyButton.setOnClickListener {
           setFragment(2)
        }
        personButton.setOnClickListener {
           setFragment(3)
        }

    }

    private fun setFragment(i: Int) {
        when(i){
            0 -> {
                nameFragment.text = "Акции"
                loadFragment(ActionsFragment.newInstance())
                setIconColor(0)
                Hawk.put("I", 0)
            }
            1 -> {
                nameFragment.text = "Чеки"
                loadFragment(CheqHistoryFragment.newInstance())
                setIconColor(1)
                Hawk.put("I", 1)
            }
            2 -> {
                loadFragment(WalleyFragment.newInstance())
                nameFragment.text = "Счёт"
                setIconColor(2)
                Hawk.put("I", 2)
            }
            3 -> {
                nameFragment.text = "Профиль"
                loadFragment(MenuFragment.newInstance())
                setIconColor(3)
                Hawk.put("I", 3)
            }
        }

    }

    private fun setIconColor(i:Int) {
        when(i) {
            0 -> {

                setDefaultIconColors()
                action_text.textColor = Color.parseColor("#f41d1d")
                action_pic.setColorFilter(Color.parseColor("#f41d1d"))

            }
            1 -> {
                setDefaultIconColors()
                history_text.textColor = Color.parseColor("#f41d1d")
                histori_pic.setColorFilter(Color.parseColor("#f41d1d"))
            }
            2 -> {
                setDefaultIconColors()
                money_text.textColor = Color.parseColor("#f41d1d")
                money_pic.setColorFilter(Color.parseColor("#f41d1d"))
            }
            3 -> {
                setDefaultIconColors()
                profile_text.textColor = Color.parseColor("#f41d1d")
                profile_pic.setColorFilter(Color.parseColor("#f41d1d"))
            }

        }
    }

    private fun setDefaultIconColors() {
        action_text.textColor = Color.parseColor("#000000")

        history_text.textColor = Color.parseColor("#000000")
        money_text.textColor = Color.parseColor("#000000")
        profile_text.textColor = Color.parseColor("#000000")

        action_pic.setColorFilter(Color.parseColor("#000000"))
        histori_pic.setColorFilter(Color.parseColor("#000000"))
        money_pic.setColorFilter(Color.parseColor("#000000"))
        profile_pic.setColorFilter(Color.parseColor("#000000"))
    }

    fun testPassName() {

        if (!(Hawk.contains(PHONE) && Hawk.contains(NAME))) {
            val s = UUID.randomUUID().toString()
            Hawk.put(PHONE, s)
            Hawk.put(NAME, "unknown")
        }
        try {
            if (!Hawk.contains("FIREBASE_TOKEN") && Hawk.get("IS_PROD")) {
                FirebaseInstanceId.getInstance().instanceId
                        .addOnCompleteListener(OnCompleteListener { task ->
                            if (!task.isSuccessful) {
                                return@OnCompleteListener
                            }

                            // Get new Instance ID token
                            val token = task.result!!.token
                            Log.d("TOKEN", "----------------->  $token")
                            // Log and toast
                            val json = jsonObject(
                                    "method" to "bill_api.set_firebase_token",
                                    "params" to jsonArray(
                                            token, Hawk.get(PHONE)
                                    ),
                                    "id" to 1

                            )
                            val url = SERVER
                            url.httpPost()
                                    .body(json.toString())
                                    .responseJson { request, response, result ->
                                        when (result) {
                                            is Result.Success -> Hawk.put("FIREBASE_TOKEN", token)
                                            is Result.Failure -> toast("Подключите устройство к интернету")
                                        }
                                    }

                        })
            } else {
                Log.d("TOKEN------------>", "                   ${Hawk.get<String>("FIREBASE_TOKEN")}")
            }
        } catch (e: Exception) {
            Log.d("TOKEN------------>", "${Hawk.get<String>("FIREBASE_TOKEN")}")
        }
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = fragmentManager.beginTransaction()

        transaction.replace(R.id.fragmentContainerMain, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }


    private fun requestCameraPermission() {
        val permissions = arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, 2)
        }
    }

    override fun onResume() {
        Hawk.init(this).build()

        testPassName()
        nameFragment.text = "Акции"
        loadFragment(ActionsFragment.newInstance())

        val i = Hawk.get<Int>("I")
        setFragment(i)
        super.onResume()
    }




    override fun onBackPressed() {
        finish()
    }

}
